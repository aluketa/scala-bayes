package bayes

import bayes.util.Bisect.bisect

import scala.math.Ordering
import scala.util.Random

object CumulativeDistributionFunction {
  def apply[T](data: Seq[(T, Double)]): CumulativeDistributionFunction[T] = {
    val total = data.map(_._2).sum
    val (values, probabilities) = data.foldLeft((0.0, (Seq[T](), Seq[Double]()))) {
      case ((runningTotal, (vs, ps)), (value, probability)) =>
        val cumulativeProbability = if (total > 0) (runningTotal + probability) / total else 0.0
        (runningTotal + probability, (vs :+ value, ps :+ cumulativeProbability))
    }._2

    new CumulativeDistributionFunction[T](values, probabilities)
  }
}

class CumulativeDistributionFunction[T](values: Seq[T], probabilities: Seq[Double]) {

  def probability(value: T)(implicit ord: Ordering[T]): Double = {
    if (ord.compare(value, values.head) < 0) {
      0.0
    } else {
      val index = bisect(values, value)
      probabilities(index - 1)
    }
  }

  def percentile(percentage: Int): T = {
    val index = bisect(probabilities, percentage / 100.0)
    values(index)
  }

  def value(probability: Double): T = {
    assert(probability >= 0.0 && probability <= 1.0, () => "Probability must be in the range 0.0 - 1.0")
    if (values.size == 0)
      throw new NoSuchElementException

    probability match {
      case 0.0 => values.head
      case 1.0 => values.last
      case _ =>
        val index = bisect(probabilities, probability) //(NearlyDouble)
        if (index > 0 && probabilities(index - 1) == probability)
          values(index - 1)
        else
          values(index)

    }
  }

  def items: Seq[(T, Double)] = values.zip(probabilities)

  def random(implicit rnd: Random = new Random()): T = {
    if (values.size == 0)
      throw new NoSuchElementException

    value(rnd.nextDouble())
  }

  def sample(n: Int)(implicit rnd: Random = new Random()): Seq[T] = for (i <- 1 to n) yield random(rnd)

  def max(k: Int): CumulativeDistributionFunction[T] = {
    new CumulativeDistributionFunction[T](values, probabilities.map(p => Math.pow(p, k)))
  }

  def credibleInterval(percentage: Double = 90.0): (T, T) = {
    assert(percentage >= 0.0 && percentage <= 100, () => "Percentage must be a value between 0.0 and 100.0")

    val probability = (1.0 - (percentage / 100.0)) / 2.0
    (value(probability), value(1.0 - probability))
  }

  def toPmf: ProbabilityMassFunction[T] = items.foldLeft((0.0, new ProbabilityMassFunction[T]())) {
    case ((previous, result), (value, probability)) =>
      (probability, result.increment(value, probability - previous))
  }._2

}
