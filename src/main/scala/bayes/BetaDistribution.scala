package bayes

import Numeric.Implicits._
import bayes.util.RichRandom._

import scala.math._
import scala.util.Random

class BetaDistribution[T](val alpha: T, val beta: T)(implicit num: Numeric[T]) {
  def mean: Double = num.toDouble(alpha) / (num.toDouble(alpha) + num.toDouble(beta))

  def update(data: (T, T)): BetaDistribution[T] = new BetaDistribution(alpha + data._1, beta + data._2)

  def random(implicit rnd: Random = new Random()): Double = rnd.betaVariate(alpha.toDouble(), beta.toDouble())

  def sample(n: Int)(implicit rnd: Random = new Random()): Seq[Double] = {
    def stream: Stream[Double] = random(rnd) #:: stream
    stream.take(n).toSeq
  }

  def pdf(x: Double): Double = pow(x, alpha.toDouble() - 1.0) * pow(1.0 - x, beta.toDouble() - 1.0)

  val pmf: ProbabilityMassFunction[T] = {
    //TODO Will not work where alpha, beta < 1.0
    val items: Seq[(T, Double)] = (1 to 100).map { i =>
      val value = div(num.fromInt(i), num.fromInt(100))
      (value, pdf(value.toDouble()))
    }

    new ProbabilityMassFunction[T](items).normalise
  }
}
