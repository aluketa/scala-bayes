package bayes.util

import bayes.div

import Numeric.Implicits._
import Ordering.Implicits._

class Interpolator[T](xs: Seq[T], ys: Seq[T])(implicit num: Numeric[T]) {

  def lookup(x: T): T = {
    bisect(x, this.xs, this.ys)
  }

  def reverse(y: T): T = {
    bisect(y, this.ys, this.xs)
  }

  private def bisect(x: T, xs: Seq[T], ys: Seq[T]): T = {
    if (x <= xs.head) {
      ys.head
    } else if (x >= xs.last) {
      ys.last
    } else {
      val i = Bisect.bisect(xs, x)
      val fraction = div(x - xs(i - 1), xs(i) - xs(i - 1))

      ys(i - 1) + (fraction * (ys(i) - ys(i - 1)))
    }
  }
}
