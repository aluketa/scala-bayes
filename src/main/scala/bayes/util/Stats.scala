package bayes.util

import scala.math._
import scala.util.Random

object Stats {

  def sampleCovariance[T](sample: Seq[T])(implicit num: Numeric[T]): Double = {
    val m = mean(sample)
    val product = sample.foldLeft(0.0)((result, t) => result + pow(num.toDouble(t) - m, 2))
    product * (1.0 / (sample.size - 1.0))
  }

  def std[T](values: Seq[T])(implicit num: Numeric[T]): Double = {
    val m = mean(values)
    sqrt(values.foldLeft(0.0)((result, t) => result + pow(num.toDouble(t) - m, 2)) / values.size)
  }

  def linspace(low: Double, high: Double, n: Int): Seq[Double] = {
    val step = (high - low) / (n - 1)
    Range.Double.inclusive(low, high, step)
  }

  def mean[T](values: Seq[T])(implicit num: Numeric[T]): Double =
    values.foldLeft(0.0)((result, t) => result + num.toDouble(t)) / values.size

  def erf(x: Double): Double = {
    val sign = if (x < 0) -1 else 1
    val p = 0.3275911
    val a1 = 0.254829592
    val a2 = -0.284496736
    val a3 = 1.421413741
    val a4 = -1.453152027
    val a5 = 1.061405429

    val t = 1.0 / (1.0 + (p * abs(x)))
    val alpha = (a1 * t) + (a2 * pow(t, 2)) + (a3 * pow(t, 3)) + (a4 * pow(t, 4)) + (a5 * pow(t, 5))
    val expMulti = exp(pow(x, 2) * -1)

    (1 - (alpha * expMulti)) * sign
  }

  def factorial(n: Int): Double = Range.inclusive(1, n).foldLeft(1.0)((r, x) => r * x)

  def bigFactorial(n: Int): BigDecimal = Range.inclusive(1, n).foldLeft(BigDecimal(1.0)) {
    (r, x) => r * x
  }

  object Norm {
    def standardCdf(x: Double): Double = (erf(x / sqrt(2)) + 1.0) / 2.0

    def cdf(x: Double, mean: Double = 0.0, sigma: Double = 1.0): Double = standardCdf((x - mean) / sigma)

    def logpdf(x: Double, mean: Double, sigma: Double): Double =
      -pow(mean - x, 2.0) / (2 * pow(sigma, 2.0)) - (0.5 * log(2 * Pi)) + log(1.0 / sigma)

    def pdf(x: Double, mean: Double, sigma: Double): Double =
      (1.0 / (sigma * sqrt(2.0 * Pi))) * exp(pow(x - mean, 2.0) / (2.0 * pow(sigma, 2.0)) * -1.0)

    def random(mean: Double = 0.0, sigma: Double = 1.0)(implicit rnd: Random = new Random): Double =
      mean + (rnd.nextGaussian() * sigma)
  }

  //http://en.wikipedia.org/wiki/Binomial_distribution
  object Binomial {
    def pmf(k: Int, n: Int, p: Double): Double = {
      binomialCoefficient(n, k) * pow(p, k) * pow(1 - p, n - k)
    }

    //http://en.wikipedia.org/wiki/Binomial_coefficient#Binomial_coefficient_in_programming_languages
    def binomialCoefficient(n: Int, k: Int): Double = {
      if (k < 0 || k > n) {
        0.0
      } else if (k == 0 || k == n) {
        1.0
      } else {
        val symK = min(k, n - k)
        var c: Double = 1.0
        for (i <- 0 to symK - 1) {
          c = c * (n.toDouble - i.toDouble) / (i.toDouble + 1.0)
        }
        c
      }

    }
  }
}
