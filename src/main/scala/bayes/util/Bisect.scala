package bayes.util

import scala.math.Ordering

object Bisect {
  def bisect[T](data: Seq[T], value: T)(implicit ord: Ordering[T]): Int = {
    bisect(data, value, 0, data.length - 1)
  }

  private def bisect[T](data: Seq[T], value: T, start: Int, end: Int)(implicit ord: Ordering[T]): Int = {
    if (ord.compare(data(start), value) > 0) {
      start
    } else if (start == end) {
      start + 1
    } else {
      val midPoint = ((end - start) / 2) + start
      if (ord.equiv(data(midPoint), value)) {
        midPoint + 1
      } else if (ord.compare(value, data(midPoint)) < 0) {
        bisect(data, value, start, midPoint)
      } else {
        bisect(data, value, midPoint + 1, end)
      }
    }
  }
}
