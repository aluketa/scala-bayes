package bayes.util

import scala.math._
import scala.util.Random

object RichRandom {

  implicit class RichRandom(val self: Random) {

    private val SgMagicConstant = 1.0 + log(4.5)

    /*
      This version due to Janne Sinkkonen, and matches all the std texts
      (e.g., Knuth Vol 2 Ed 3 pg 134 "the beta distribution").
      (Derived from Python's implementation)
     */
    def betaVariate(alpha: Double, beta: Double): Double = {
      val a = gammaVariate(alpha, 1.0)
      val b = gammaVariate(beta, 1.0)

      a / (a + b)
    }

    //Derived from Python's implementation
    def gammaVariate(alpha: Double, beta: Double): Double = {
      if (alpha <= 0.0 || beta <= 0.0) {
        throw new RuntimeException("alpha and beta must be greater than zero")
      }

      if (alpha > 1.0) {
        gammaVariateAlphaGreaterThanZero(alpha, beta)
      } else if (alpha == 1.0) {
        gammaVariateAlphaOne(beta)
      } else {
        gammaVariateAlphaZeroToOne(alpha, beta)
      }
    }

    /*
      Uses R.C.H. Cheng, "The generation of Gamma variables with non-integral shape parameters",
      Applied Statistics, (1977), 26, No. 1, p71-74
    */
    private def gammaVariateAlphaGreaterThanZero(alpha: Double, beta: Double): Double = {
      val ainv = sqrt(2.0 * alpha - 1.0)
      val bbb = alpha - log(4)
      val ccc = alpha + ainv

      while (true) {
        val u1 = self.nextDouble()
        if (notZeroOrOne(u1)) {
          val u2 = 1.0 - self.nextDouble()
          val v = log(u1 / (1.0 - u1)) / ainv
          val x = alpha * exp(v)
          val z = u1 * u1 * u2
          val r = bbb + ccc * v - x
          if (r + SgMagicConstant - 4.5 * z >= 0.0 || r >= log(z)) {
            return x * beta
          }
        }
      }
      throw new RuntimeException("Error in gammaVariateAlphaGreaterThanZero")
    }

    private def gammaVariateAlphaOne(beta: Double): Double = {
      var u = self.nextDouble()
      while (u <= 1e-7)
        u = self.nextDouble()

      -log(u) * beta
    }

    /*
      Uses ALGORITHM GS of Statistical Computing - Kennedy & Gentle
     */
    private def gammaVariateAlphaZeroToOne(alpha: Double, beta: Double): Double = {
      while (true) {
        val u = self.nextDouble()
        val b = (E + alpha) / E
        val p = b * u
        val x = if (p <= 1.0) pow(p, 1.0 / alpha) else -log((b - p) / alpha)
        val u1 = self.nextDouble()
        if (p > 1.0) {
          if (u1 <= pow(x, alpha - 1.0)) {
            return x * beta
          } else if (u1 <= exp(-x)) {
            return x * beta
          }
        }
      }
      throw new RuntimeException("Error in gammaVariateAlphaZeroToOne")
    }

    private def notZeroOrOne(x: Double): Boolean = 1e-7 < x && x < 0.9999999
  }

}