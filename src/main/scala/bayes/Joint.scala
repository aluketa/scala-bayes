package bayes

import scala.language.implicitConversions

object Joint {
  implicit def tuple2PmfToJoint[T](pmf: ProbabilityMassFunction[(T, T)]): Joint[T, (T, T)] = {
    new JointPmf[T, (T, T)](pmf) {
      override def valuesToSeq(values: (T, T)): Seq[T] = Seq(values._1, values._2)
    }
  }

  implicit def seqPmfToJoint[T](pmf: ProbabilityMassFunction[Seq[T]]): Joint[T, Seq[T]] = {
    new JointPmf[T, Seq[T]](pmf) {
      override def valuesToSeq(values: Seq[T]): Seq[T] = values
    }
  }

  def from[T](pmf1: ProbabilityMassFunction[T], pmf2: ProbabilityMassFunction[T]): Joint[T, (T, T)] = {
    pmf1.items.foldLeft(new ProbabilityMassFunction[(T, T)]) {
      case (result1, (v1, p1)) => pmf2.items.foldLeft(result1) {
        case (result2, (v2, p2)) => result2.set((v1, v2), p1 * p2)
      }
    }
  }
}

trait Joint[T, CollectionT] {
  def targetPmf: ProbabilityMassFunction[CollectionT]

  def valuesToSeq(values: CollectionT): Seq[T]

  def marginal(index: Int): ProbabilityMassFunction[T] = {
    targetPmf.items.foldLeft(new ProbabilityMassFunction[T]) {
      case (result, (values, probability)) => result.increment(valuesToSeq(values)(index), probability)
    }
  }

  def conditional(index: Int, conditionalIndex: Int, conditionalValue: T): ProbabilityMassFunction[T] = {
    targetPmf.items
      .map { case (values, probability) => (valuesToSeq(values), probability)}
      .filter { case (valueSeq, _) => valueSeq(conditionalIndex) == conditionalValue}
      .foldLeft(new ProbabilityMassFunction[T]) {
      case (result, (valueSeq, probability)) => result.increment(valueSeq(index), probability)
    }
      .normalise
  }
}

trait JointSeq[T] extends Joint[T, Seq[T]] {
  suite: BayesianSuite[Seq[T], _] =>
  override def targetPmf: ProbabilityMassFunction[Seq[T]] = suite
  override def valuesToSeq(values: Seq[T]): Seq[T] = values
}

trait Joint2[T] extends Joint[T, (T, T)] {
  suite: BayesianSuite[(T, T), _] =>
  override def targetPmf: ProbabilityMassFunction[(T, T)] = suite
  override def valuesToSeq(values: (T, T)): Seq[T] = Seq(values._1, values._2)
}

private abstract class JointPmf[T, CollectionT](pmf: ProbabilityMassFunction[CollectionT]) extends Joint[T, CollectionT] {
  override def targetPmf: ProbabilityMassFunction[CollectionT] = pmf
}