package bayes.mutable

import java.util.{ArrayList => JavaArrayList}
import java.util.concurrent.ConcurrentHashMap

import scala.collection.JavaConversions._

class ProbabilityMassFunction[T](
                                  private val probabilityMap: ConcurrentHashMap[T, Double],
                                  private val orderOfInsertion: JavaArrayList[T] = new JavaArrayList[T]()) {

  private val nullValue: T = null.asInstanceOf[T]

  def this() = this(new ConcurrentHashMap[T, Double])

  def this(d: Seq[(T, Double)]) = this(new ConcurrentHashMap[T, Double](d.toMap), new JavaArrayList(d.map(_._1)))

  def set(value: T, probability: Double): ProbabilityMassFunction[T] = {
    if (probabilityMap.put(value, probability) == nullValue) {
      orderOfInsertion.add(value)
    }
    this
  }

  def increment(value: T, probability: Double): ProbabilityMassFunction[T] = {
    val previousValue = probabilityMap.getOrDefault(value, 0.0)
    if (probabilityMap.put(value, previousValue + probability) == nullValue) {
      orderOfInsertion.add(value)
    }
    this
  }

  def multiply(value: T, factor: Double): ProbabilityMassFunction[T] = {
    val previousValue = probabilityMap.getOrDefault(value, 0.0)
    if (probabilityMap.put(value, previousValue * factor) == nullValue) {
      orderOfInsertion.add(value)
    }
    this
  }

  def total: Double = probabilityMap.values().sum

  def normalise(): ProbabilityMassFunction[T] = {
    val priorTotal = total
    if (priorTotal > 0) {
      probabilityMap.keys.foreach(k => multiply(k, 1.0 / priorTotal))
    }

    this
  }

  def probability(value: T): Double = probabilityMap(value)

  def items: Seq[(T, Double)] = orderOfInsertion.map(key => (key, probabilityMap(key)))

  def values: Seq[T] = orderOfInsertion

  def logarithmic: LogarithmicProbabilityMassFunction[T] = {
    val max = probabilityMap.map(_._2).max
    val map = new ConcurrentHashMap[T, Double]
    val orderList = new JavaArrayList[T]

    items
      .filter { case (_, probability) => probability != 0.0}
      .foreach {
      case (value, probability) =>
        map.put(value, math.log(probability / max))
        orderList.add(value)
    }

    new LogarithmicProbabilityMassFunction[T](map, orderList)
  }
}

class LogarithmicProbabilityMassFunction[T](
                                             private val probabilityMap: ConcurrentHashMap[T, Double],
                                             private val orderOfInsertion: JavaArrayList[T] = new JavaArrayList[T]()) {

  def this() = this(new ConcurrentHashMap[T, Double])

  private val nullValue: T = null.asInstanceOf[T]

  def increment(value: T, probability: Double): LogarithmicProbabilityMassFunction[T] = {
    val previousValue = probabilityMap.getOrDefault(value, 0.0)
    if (probabilityMap.put(value, previousValue + probability) == nullValue) {
      orderOfInsertion.add(value)
    }
    this
  }

  def values: Seq[T] = orderOfInsertion

  def items: Seq[(T, Double)] = orderOfInsertion.map(key => (key, probabilityMap(key)))

  def exp(): ProbabilityMassFunction[T] = {
    val map = new ConcurrentHashMap[T, Double]()
    val max = probabilityMap.map(_._2).max

    items.foreach {
      case (value, probability) =>
        map.put(value, math.exp(probability - max))
    }

    new ProbabilityMassFunction[T](map, orderOfInsertion)
  }
}
