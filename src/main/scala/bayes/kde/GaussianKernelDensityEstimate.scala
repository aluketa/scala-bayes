package bayes.kde

import bayes.util.Stats.sampleCovariance

import scala.math.{pow, sqrt, exp}
import scala.math.Pi

class GaussianKernelDensityEstimate[T](sample: Seq[T])(implicit num: Numeric[T]) {
  val scottsFactor = pow(sample.size, -1.0 / 5.0)
  val dataCovariance = sampleCovariance(sample)
  val covariance = dataCovariance * pow(scottsFactor, 2.0)
  val inverseCovariance = (1.0 / dataCovariance) / pow(scottsFactor, 2.0)
  val normalisingFactor = sqrt(2 * Pi * covariance) * sample.size

  def evaluate(value: T): Double = {
    val result = sample.foldLeft(0.0) { case (r, t) =>
      val diff = num.toDouble(t) - num.toDouble(value)
      val tDiff = diff * inverseCovariance
      val energy = tDiff * (diff / 2.0)

      r + exp(energy * -1.0)
    }

    result / normalisingFactor
  }

}
