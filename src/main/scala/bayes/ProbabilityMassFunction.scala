package bayes

import scala.collection.immutable.ListMap
import scala.util.Random

object ProbabilityMassFunction {
  def fromSeq[T](data: Seq[T]): ProbabilityMassFunction[T] = {
    data.foldLeft(new ProbabilityMassFunction[T]()) { case (result, x) => result.increment(x, 1.0)}.normalise
  }

  def sampleSum[T](pmfs: Seq[ProbabilityMassFunction[T]], n: Int)(implicit num: Numeric[T]): ProbabilityMassFunction[T] =
    fromSeq((1 to n).map(_ => randomSum(pmfs)))

  private def randomSum[T](pmfs: Seq[ProbabilityMassFunction[T]])(implicit num: Numeric[T]): T = {
    pmfs.foldLeft(null.asInstanceOf[T]) {
      case (result, pmf) =>
        if (result == null)
          pmf.random
        else
          num.plus(result, pmf.random)
    }
  }

  def sampleMax[T](pmfs: Seq[ProbabilityMassFunction[T]], n: Int)(implicit num: Numeric[T]): ProbabilityMassFunction[T] = {
    fromSeq((1 to n).map(_ => randomMax(pmfs)))
  }

  private def randomMax[T](pmfs: Seq[ProbabilityMassFunction[T]])(implicit num: Numeric[T]): T =
    pmfs.map(pmf => pmf.random).max

  def makeMixture[T](metaPmf: ProbabilityMassFunction[ProbabilityMassFunction[T]]): ProbabilityMassFunction[T] = {
    metaPmf.items.foldLeft(new ProbabilityMassFunction[T]()) { case (resultPmf, (currentPmf, p1)) =>
      currentPmf.items.foldLeft(resultPmf) { case (result, (value, p2)) =>
        result.increment(value, p1 * p2)
      }
    }.normalise
  }

  def probabilityLessThan[T](pmf1: ProbabilityMassFunction[T], pmf2: ProbabilityMassFunction[T])(implicit ord: Ordering[T]): Double = {
    (for ((v1, p1) <- pmf1.items; (v2, p2) <- pmf2.items; if ord.lt(v1, v2)) yield p1 * p2).sum
  }

  def probabilityGreaterThan[T](pmf1: ProbabilityMassFunction[T], pmf2: ProbabilityMassFunction[T])(implicit ord: Ordering[T]): Double = {
    (for ((v1, p1) <- pmf1.items; (v2, p2) <- pmf2.items; if ord.gt(v1, v2)) yield p1 * p2).sum
  }
}

class ProbabilityMassFunction[T](private val probabilityMap: ListMap[T, Double]) {
  def this(d: Seq[(T, Double)]) = this(ListMap(d: _*))

  def this() = this(Seq())

  def this(m: Map[T, Double]) = this(m.toSeq)

  protected def apply(m: ListMap[T, Double]): ProbabilityMassFunction[T] = new ProbabilityMassFunction[T](m)

  protected def apply(d: Seq[(T, Double)]): ProbabilityMassFunction[T] = apply(ListMap(d: _*))

  def set(value: T, probability: Double): ProbabilityMassFunction[T] = {
    apply(probabilityMap + (value -> probability))
  }

  def increment(value: T, probability: Double): ProbabilityMassFunction[T] = {
    val previousValue: Double = probabilityMap.getOrElse(value, 0)
    apply(probabilityMap + (value -> (previousValue + probability)))
  }

  def multiply(value: T, factor: Double): ProbabilityMassFunction[T] = {
    val previousValue: Double = probabilityMap.getOrElse(value, 0)
    apply(probabilityMap + (value -> (previousValue * factor)))
  }

  def total: Double = probabilityMap.values.sum

  def values: Seq[T] = probabilityMap.keys.toSeq

  def normalise: ProbabilityMassFunction[T] = {
    val priorTotal = total
    if (priorTotal > 0) {
      val pmf = probabilityMap.keys.foldLeft(this)((result, key) => result.multiply(key, 1.0 / priorTotal))
      apply(pmf.items)
    } else {
      this
    }
  }

  def items: Seq[(T, Double)] = probabilityMap.toSeq

  def probability(value: T): Double = probabilityMap(value)

  def max: T = {
    def sortedByProbability: Seq[(T, Double)] =
      probabilityMap.toList.sortWith { case ((_, p1), (_, p2)) => p1 > p2}

    sortedByProbability match {
      case (v, _) :: _ => v
      case _ => null.asInstanceOf[T]
    }
  }

  def percentile(percentage: Int): T = {
    var total = 0.0
    probabilityMap.foreach { case (value, probability) =>
      total += probability
      if (total >= (percentage / 100.0))
        return value
    }

    null.asInstanceOf[T]
  }

  def random(implicit rnd: Random = new Random): T = {
    if (probabilityMap.size == 0)
      throw new NoSuchElementException

    val target = rnd.nextDouble()
    var total = 0.0
    items.foreach { case (value, probability) =>
      total += probability
      if (total >= target)
        return value
    }

    throw new NoSuchElementException
  }

  def +(other: ProbabilityMassFunction[T])(implicit num: Numeric[T]): ProbabilityMassFunction[T] = this.add(other)

  def add(other: ProbabilityMassFunction[T])(implicit num: Numeric[T]): ProbabilityMassFunction[T] =
    combine(other, (v1, v2) => num.plus(v1, v2))

  def -(other: ProbabilityMassFunction[T])(implicit num: Numeric[T]): ProbabilityMassFunction[T] = this.subtract(other)

  def subtract(other: ProbabilityMassFunction[T])(implicit num: Numeric[T]): ProbabilityMassFunction[T] =
    combine(other, (v1, v2) => num.minus(v1, v2))

  def max(other: ProbabilityMassFunction[T])(implicit num: Numeric[T]): ProbabilityMassFunction[T] =
    combine(other, (v1, v2) => num.max(v1, v2))

  def max(k: Int): ProbabilityMassFunction[T] = this.toCdf.max(k).toPmf

  private def combine(other: ProbabilityMassFunction[T], combiner: (T, T) => T)(implicit num: Numeric[T]): ProbabilityMassFunction[T] = {
    val keys = for (v1 <- this.values; v2 <- other.values) yield (v1, v2)

    keys.foldLeft(new ProbabilityMassFunction[T]) { case (result, (v1, v2)) =>
      val p1 = this.probability(v1)
      val p2 = other.probability(v2)
      result.increment(combiner(v1, v2), p1 * p2)
    }
  }

  def probabilityGreaterThan(value: T)(implicit ord: Ordering[T]): Double = {
    items.filter { case (v, _) => ord.gt(v, value)}
      .foldLeft(0.0) { case (result, (_, p)) => result + p}
  }

  def probabilityLessThan(value: T)(implicit ord: Ordering[T]): Double = {
    items.filter { case (v, _) => ord.lt(v, value)}
      .foldLeft(0.0) { case (result, (_, p)) => result + p}
  }

  def probabilityGreaterThan(otherPmf: ProbabilityMassFunction[T])(implicit ord: Ordering[T]): Double = {
    (for ((v1, p1) <- items; (v2, p2) <- otherPmf.items; if ord.gt(v1, v2)) yield p1 * p2).sum
  }

  def probabilityLessThan(otherPmf: ProbabilityMassFunction[T])(implicit ord: Ordering[T]): Double = {
    (for ((v1, p1) <- items; (v2, p2) <- otherPmf.items; if ord.lt(v1, v2)) yield p1 * p2).sum
  }

  def probabilityEqual(otherPmf: ProbabilityMassFunction[T])(implicit ord: Ordering[T]): Double = {
    (for ((v1, p1) <- items; (v2, p2) <- otherPmf.items; if ord.equiv(v1, v2)) yield p1 * p2).sum
  }

  def mean(implicit num: Numeric[T]): Double = items.map { case (v, p) => num.toDouble(v) * p}.sum

  def size: Int = probabilityMap.size

  def toCdf: CumulativeDistributionFunction[T] = CumulativeDistributionFunction[T](this.probabilityMap.toSeq)

  def bias(implicit num: Numeric[T]): ProbabilityMassFunction[T] = {
    this.items.foldLeft(this) { case (result, (value, probability)) =>
      result.multiply(value, num.toDouble(value))
    }.normalise
  }

  def unbias(implicit num: Numeric[T]): ProbabilityMassFunction[T] = {
    this.items.foldLeft(this) { case (result, (value, probability)) =>
      result.multiply(value, 1.0 / num.toDouble(value))
    }.normalise
  }

  def maximumLikelihood: T = {
    this.items.foldLeft((null.asInstanceOf[T], 0.0)) {
      case ((resultValue, resultProbability), (currentValue, currentProbability)) =>
        if (currentProbability > resultProbability || resultValue == null.asInstanceOf[T])
          (currentValue, currentProbability)
        else
          (resultValue, resultProbability)
    }._1
  }

  def maximumProbability: Double = probabilityMap.values.max

  def maximumLikelihoodInterval(percentage: Double = 90.0): Seq[T] = {
    assert(percentage >= 0.0 && percentage <= 100, () => "Percentage must be a value between 0.0 and 100.0")
    val target = percentage / 100.0

    this.items.sortBy(_._2).reverse.foldLeft((Seq[T](), 0.0)) {
      case ((interval, total), (value, probability)) =>
        if (total < target) {
          (interval :+ value, total + probability)
        } else {
          (interval, total)
        }
    }._1
  }

  def logarithmic: LogarithmicProbabilityMassFunction[T] = {
    val max = maximumProbability
    val logData: Seq[(T, Double)] = items
      .filter { case (_, probability) => probability != 0.0}
      .map { case (value, probability) => (value, math.log(probability / max))}

    new LogarithmicProbabilityMassFunction[T](ListMap(logData: _*))
  }


  override def equals(obj: scala.Any): Boolean = obj match {
    case pmf: ProbabilityMassFunction[T] => probabilityMap.equals(pmf.probabilityMap)
    case _ => false
  }

  override def hashCode(): Int = probabilityMap.hashCode()

  override def toString: String = probabilityMap.toString()
}

class LogarithmicProbabilityMassFunction[T](private val probabilityMap: ListMap[T, Double]) {
  def increment(value: T, probability: Double): LogarithmicProbabilityMassFunction[T] = {
    val previousValue: Double = probabilityMap.getOrElse(value, 0)
    new LogarithmicProbabilityMassFunction[T](probabilityMap + (value -> (previousValue + probability)))
  }

  def values: Seq[T] = probabilityMap.keys.toSeq

  def items: Seq[(T, Double)] = probabilityMap.toSeq

  def exp: ProbabilityMassFunction[T] = {
    val max = probabilityMap.map(_._2).max
    val data: Seq[(T, Double)] = items.map {
      case (value, probability) => (value, math.exp(probability - max))
    }
    new ProbabilityMassFunction[T](data)
  }
}
