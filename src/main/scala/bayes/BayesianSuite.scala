package bayes

import bayes.mutable.{
ProbabilityMassFunction => MutableProbabilityMassFunction,
LogarithmicProbabilityMassFunction => MutableLogarithmicProbabilityMassFunction}

import scala.language.implicitConversions
import scala.math.log

object BayesianSuite {
  implicit def suiteToPmf[Hypothesis, _](suite: BayesianSuite[Hypothesis, _]): ProbabilityMassFunction[Hypothesis] = {
    suite.toPmf
  }
}

trait BayesianSuite[Hypothesis, Data] {
  protected type TargetType = this.type

  def toPmf: ProbabilityMassFunction[Hypothesis]

  def fromPmf(pmf: ProbabilityMassFunction[Hypothesis]): TargetType

  def likelihood(data: Data, hypothesis: Hypothesis): Double

  def update(data: Data, normalise: Boolean = true): TargetType = {
    if (normalise) {
      fromPmf(updateWithoutNormalise(toPmf, data).normalise)
    } else {
      fromPmf(updateWithoutNormalise(toPmf, data))
    }
  }

  def updateAll(data: Seq[Data], useMutableIteration: Boolean = false): TargetType = {
    if (useMutableIteration) {
      updateAllMutable(data)
    } else {
      val updatedPmf = data.foldLeft(new ProbabilityMassFunction[Hypothesis](toPmf.items)) {
        (result, d) =>
          updateWithoutNormalise(result, d)
      }

      fromPmf(updatedPmf.normalise)
    }
  }

  def normalise: TargetType = fromPmf(toPmf.normalise)

  def logarithmic(
                   logLikelihood: (Data, Hypothesis) => Double = (d, h) => log(likelihood(d, h))): LogarithmicBayesianSuite =
    new LogarithmicBayesianSuite(toPmf.logarithmic, logLikelihood)

  private def updateAllMutable(data: Seq[Data]): TargetType = {
    val updatedPmf = data.foldLeft(new MutableProbabilityMassFunction[Hypothesis](toPmf.items)) {
      (result, d) => updateWithoutNormaliseMutable(result, d)
    }

    fromPmf(new ProbabilityMassFunction[Hypothesis](updatedPmf.normalise().items))
  }

  private def updateWithoutNormalise(pmf: ProbabilityMassFunction[Hypothesis], data: Data): ProbabilityMassFunction[Hypothesis] = {
    pmf.values.foldLeft(pmf) { (result, hypothesis) =>
      val like = likelihood(data, hypothesis)
      result.multiply(hypothesis, like)
    }
  }

  private def updateWithoutNormaliseMutable(pmf: MutableProbabilityMassFunction[Hypothesis], data: Data): MutableProbabilityMassFunction[Hypothesis] = {
    pmf.values.foldLeft(pmf) { (result, hypothesis) =>
      val like = likelihood(data, hypothesis)
      result.multiply(hypothesis, like)
    }
  }

  class LogarithmicBayesianSuite(
                                  private val pmf: LogarithmicProbabilityMassFunction[Hypothesis],
                                  private val logLikelihood: (Data, Hypothesis) => Double) {

    def logUpdate(data: Data): LogarithmicBayesianSuite = {
      val updatedPmf = pmf.values.foldLeft(pmf) { (result, hypothesis) =>
        val like = logLikelihood(data, hypothesis)
        result.increment(hypothesis, like)
      }
      new LogarithmicBayesianSuite(updatedPmf, logLikelihood)
    }

    def logUpdateAll(data: Seq[Data], useMutableIteration: Boolean = false): LogarithmicBayesianSuite = {
      if (useMutableIteration) {
        logUpdateAllMutable(data)
      } else {
        data.foldLeft(this) { case (result, d) => result.logUpdate(d)}
      }
    }

    private def logUpdateAllMutable(data: Seq[Data]): LogarithmicBayesianSuite = {
      val updatedPmf = data.foldLeft(new MutableProbabilityMassFunction[Hypothesis](pmf.exp.items).logarithmic) {
        (result, d) => logUpdateMutable(result, d)
      }

      new LogarithmicBayesianSuite(new ProbabilityMassFunction(updatedPmf.exp().items).logarithmic, logLikelihood)
    }

    private def logUpdateMutable(pmf: MutableLogarithmicProbabilityMassFunction[Hypothesis], data: Data): MutableLogarithmicProbabilityMassFunction[Hypothesis] = {
      pmf.values.foldLeft(pmf) { (result, hypothesis) =>
        val like = logLikelihood(data, hypothesis)
        result.increment(hypothesis, like)
      }
    }

    def exp: TargetType = fromPmf(pmf.exp)
  }
}

object BasicBayesian {
  def apply[H, D](hypotheses: Seq[H], like: (D, H) => Double) =
    new BasicBayesian[H, D](new ProbabilityMassFunction[H](hypotheses.map(h => (h, 1.0))).normalise.items, like)
}

class BasicBayesian[Hypothesis, Data](
                                       data: Seq[(Hypothesis, Double)],
                                       like: (Data, Hypothesis) => Double)
  extends BayesianSuite[Hypothesis, Data] {

  override def toPmf: ProbabilityMassFunction[Hypothesis] = new ProbabilityMassFunction[Hypothesis](data)

  override def fromPmf(pmf: ProbabilityMassFunction[Hypothesis]): TargetType =
    new BasicBayesian[Hypothesis, Data](pmf.items, like).asInstanceOf[TargetType]

  override def likelihood(data: Data, hypothesis: Hypothesis): Double = {
    this.like(data, hypothesis)
  }
}

