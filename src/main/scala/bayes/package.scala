package object bayes {
  def div[T](t1: T, t2: T)(implicit num: Numeric[T]): T = implicitly[Numeric[T]] match {
    case num: Fractional[T] => num.div(t1, t2)
    case num: Integral[T] => num.fromInt(num.toInt(t1) / num.toInt(t2))
    case _ => throw new RuntimeException("Un-divisible numeric")
  }
}
