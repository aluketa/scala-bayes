package bayes.pdf

import bayes.ProbabilityMassFunction

import scala.math._
import bayes.util.Stats.{factorial, bigFactorial}

object PoissonProbabilityDensityFunction {
  def poissonPmf(lambda: Double, low: Int, high: Int): ProbabilityMassFunction[Int] = {
    val pdf = new PoissonProbabilityDensityFunction(lambda)
    Range.inclusive(low, high).foldLeft(new ProbabilityMassFunction[Int]()) { case (result, k) =>
      result.set(k, pdf.density(k))
    }.normalise
  }
}

class PoissonProbabilityDensityFunction(lambda: Double) extends ProbabilityDensityFunction[Int] {

  override def density(x: Int)(implicit num: scala.Numeric[Int]): Double = {
    if (x < 101)
      (pow(lambda, x) * exp(lambda * -1)) / factorial(x)
    else
      (BigDecimal(lambda).pow(x) * BigDecimal(exp(lambda * -1)) / bigFactorial(x)).toDouble
  }
}
