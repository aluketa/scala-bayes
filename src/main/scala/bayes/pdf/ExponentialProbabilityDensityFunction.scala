package bayes.pdf

import bayes.ProbabilityMassFunction
import bayes.util.Stats._

import scala.math.exp

object ExponentialProbabilityDensityFunction {
  def exponentialPmf(lambda: Double, low: Int, high: Int, count: Int): ProbabilityMassFunction[Double] = {
    val pdf = new ExponentialProbabilityDensityFunction[Double](lambda)
    pdf.toPmf(linspace(low, high, count))
  }
}

class ExponentialProbabilityDensityFunction[T](lambda: Double) extends ProbabilityDensityFunction[T] {
  override def density(x: T)(implicit num: Numeric[T]): Double = lambda * exp(lambda * -1 * num.toDouble(x))
}
