package bayes.pdf

import bayes.ProbabilityMassFunction
import bayes.util.Stats.{Norm, linspace}

object GaussianProbabilityDensityFunction {
  def gaussianPmf(mean: Double, sigma: Double, span: Int, count: Int = 101): ProbabilityMassFunction[Double] = {
    val low = mean - (span * sigma)
    val high = mean + (span * sigma)
    val pdf = new GaussianProbabilityDensityFunction[Double](mean, sigma)

    pdf.toPmf(linspace(low, high, count))
  }
}

class GaussianProbabilityDensityFunction[T](mean: Double, std: Double) extends ProbabilityDensityFunction[T] {
  override def density(x: T)(implicit num: Numeric[T]): Double = Norm.pdf(num.toDouble(x), mean, std)
}
