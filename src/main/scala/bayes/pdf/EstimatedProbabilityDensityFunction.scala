package bayes.pdf

import bayes.kde.GaussianKernelDensityEstimate

class EstimatedProbabilityDensityFunction[T](sample: Seq[T])(implicit num: Numeric[T]) extends ProbabilityDensityFunction[T] {
  private val kde = new GaussianKernelDensityEstimate[T](sample)

  override def density(x: T)(implicit num: Numeric[T]): Double = kde.evaluate(x)
}
