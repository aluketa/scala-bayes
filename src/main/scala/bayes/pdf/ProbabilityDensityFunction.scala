package bayes.pdf

import bayes.ProbabilityMassFunction

trait ProbabilityDensityFunction[T] {
  def density(x: T)(implicit num: Numeric[T]): Double

  def toPmf(data: Seq[T])(implicit num: Numeric[T]): ProbabilityMassFunction[T] =
    data.foldLeft(new ProbabilityMassFunction[T]())((pmf, d) => pmf.set(d, density(d))).normalise
}
