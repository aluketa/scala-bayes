package bayes

import bayes.mutable.{ProbabilityMassFunction => MutableProbabilityMassFunction}
import bayes.pdf.GaussianProbabilityDensityFunction
import bayes.util.Stats
import bayes.util.Stats.Norm

import scala.collection.mutable.{Map => MutableMap}
import scala.io.Source
import scala.math.log

object Variability extends App {

  val maleHeights = readHeights("/male_heights").take(1000)
  val femaleHeights = readHeights("/female_heights").take(1000)

  val maleSuite = createSuiteAbc(maleHeights)
  val femaleSuite = createSuiteAbc(femaleHeights)

  val maleCoV = coefficientOfVariation(maleSuite)
  val femaleCoV = coefficientOfVariation(femaleSuite)

  println(maleCoV.probabilityGreaterThan(femaleCoV))
  println(femaleCoV.probabilityGreaterThan(maleCoV))

  // 100                log                   log mutable         log fast              ABC                   ABC Robust
  //0.5293380303308729  0.5293380303308666    0.5293380303308668  0.5293380303308753    0.5293366187454828    0.20623974762487837
  //0.4706619696691162  0.47066196966911844   0.4706619696691186  0.47066196966911256   0.47066338125452767   0.7937602523750483

  // 1000
  //0.0                 0.7930694376173847    0.7930694376173847  0.7930694376175824    0.793012129532047     0.9219301845384003
  //0.0                 0.206930562382613     0.206930562382613   0.20693056238238977   0.20698787046795156   0.0780698154615616

  def createSuite(heights: Seq[Int]): Height = {
    val (mus, sigmas) = priorRanges(heights, 31)
    new Height(mus, sigmas)
      .updateAll(heights, useMutableIteration = true)
  }

  def createLogSuite(heights: Seq[Int]): Height = {
    val (mus, sigmas) = priorRanges(heights, 31)
    val suite = new Height(mus, sigmas)

    suite.logarithmic((data, hypothesis) => log(suite.likelihood(data, hypothesis)))
      .logUpdateAll(heights, useMutableIteration = true)
      .exp
      .normalise
  }

  def createSuiteFast(heights: Seq[Int]): Height = {
    val (mus, sigmas) = priorRanges(heights, 31)
    val suite = new Height(mus, sigmas)
    val logPmf = new MutableProbabilityMassFunction[(Double, Double)](suite.items).logarithmic

    val n = heights.size.toDouble

    val cache = MutableMap[Double, Double]()
    def summation(mu: Double): Double = {
      cache.getOrElseUpdate(mu, heights.map(x => math.pow(x - mu, 2)).sum)
    }

    suite.values.foreach { case (mu, sigma) =>
      val total = summation(mu)
      val logLike = -n * math.log(sigma) - total / (2 * math.pow(sigma, 2.0))
      logPmf.increment((mu, sigma), logLike)
    }

    new Height(new ProbabilityMassFunction(logPmf.exp().normalise().items))
  }

  def createSuiteAbc(heights: Seq[Int]): Height = {
    val (mus, sigmas) = priorRanges(heights, 31)
    val suite = new Height(mus, sigmas)
    val logPmf = new MutableProbabilityMassFunction[(Double, Double)](suite.items).logarithmic

    //val sampleMean_ = Stats.mean(heights) // 178.74
    //val sampleStd_ = Stats.std(heights)   // 7.012303473181972
    val (sampleMean, sampleStd) = medianAndStd(heights, numSigmas = 1)
    val n = heights.size.toDouble

    suite.values.foreach {
      case (mu, sigma) =>
        val errorMean = sigma / math.sqrt(n)
        val logLikeMean = Norm.logpdf(sampleMean, mu, errorMean)

        val errorStd = sigma / math.sqrt(2.0 * (n - 1.0))
        val logLikeStd = Norm.logpdf(sampleStd, sigma, errorStd)

        logPmf.increment((mu, sigma), logLikeMean + logLikeStd)
    }

    new Height(new ProbabilityMassFunction(logPmf.exp().normalise().items))
  }

  def medianAndStd(heights: Seq[Int], numSigmas: Int): (Double, Double) = {
    val halfP = Norm.standardCdf(numSigmas) - 0.5
    val (median, ipr) = medianAndInterQuartileRange(heights, halfP * 2.0)
    val s = ipr / 2.0 / numSigmas

    (median, s)
  }

  def medianAndInterQuartileRange(heights: Seq[Int], percentile: Double): (Double, Double) = {
    val cdf = ProbabilityMassFunction.fromSeq(heights.sorted).toCdf
    val median = cdf.percentile(50)

    val alpha = (1.0 - percentile) / 2.0
    val ipr = cdf.value(1.0 - alpha) - cdf.value(alpha)

    (median, ipr)
  }

  def coefficientOfVariation(suite: Height): ProbabilityMassFunction[Double] = {
    suite.items.foldLeft(new ProbabilityMassFunction[Double]) {
      case (result, ((mu, sigma), probability)) => result.increment(sigma / mu, probability)
    }
  }

  def readHeights(filename: String): Seq[Int] =
    Source.fromURL(getClass.getResource(filename)).getLines().map(_.toInt).toSeq

  def priorRanges(sample: Seq[Int], numPoints: Int, numStdErrors: Int = 3): (Seq[Double], Seq[Double]) = {
    def range(estimate: Double, spread: Double): Seq[Double] =
      Stats.linspace(estimate - spread, estimate + spread, numPoints)

    val n = sample.size.toDouble
    val m = Stats.mean(sample)
    val s = Stats.std(sample)

    val stdErrorForMean = s / math.sqrt(n)
    val mus = range(m, stdErrorForMean * numStdErrors)

    val stdErrorForStd = s / math.sqrt(2.0 * (n - 1.0))
    val sigmas = range(s, stdErrorForStd * numStdErrors)

    (mus, sigmas)
  }

  class Height(private val pmf: ProbabilityMassFunction[(Double, Double)])
    extends BayesianSuite[(Double, Double), Int] with Joint2[Double] {

    def this(mus: Seq[Double], sigmas: Seq[Double]) =
      this(ProbabilityMassFunction.fromSeq(for(mu <- mus; sigma <- sigmas) yield (mu, sigma)))

    override def toPmf: ProbabilityMassFunction[(Double, Double)] = pmf
    override def fromPmf(pmf: ProbabilityMassFunction[(Double, Double)]): TargetType = new Height(pmf).asInstanceOf[TargetType]

    override def likelihood(data: Int, hypothesis: (Double, Double)): Double = {
      val (mu, sigma) = hypothesis
      new GaussianProbabilityDensityFunction[Int](mu, sigma).density(data)
    }
  }
}
