package bayes

import scala.math.pow

object EuroHypothesis extends App {
  //val data = ("H" * 140).toSeq ++ ("T" * 110).toSeq
  //val suite = new Euro(ProbabilityMassFunction.fromSeq(0 to 100))
  //val posterier = suite.updateAll(data)

  val data = (140, 110)
  val suite = new OptimisedEuro(ProbabilityMassFunction.fromSeq(0 to 100))
  val posterier = suite.update(data)
  printResults(posterier)

  println("")

  //val triangularSuite = new Euro(trianglePrior)
  //val triangularPosterier = triangularSuite.updateAll(data)
  val triangularSuite = new OptimisedEuro(trianglePrior)
  val triangularPosterier = triangularSuite.update(data)
  printResults(triangularPosterier)

  println("")

  val likelihoodFair = suite.likelihood(data, 50)
  println(s"p(D|F): $likelihoodFair")

  val actualPercent = ((140.0 / 250.0) * 100.0).toInt
  val likelihoodBiasCheat = suite.likelihood(data, actualPercent)
  println(s"B Cheat:\t\t$likelihoodBiasCheat\t${likelihoodBiasCheat / likelihoodFair}")

  val likelihood40 = suite.likelihood(data, 40)
  val likelihood60 = suite.likelihood(data, 60)
  val likelihoodTwo = (0.5 * likelihood40) + (0.5 * likelihood60)
  println(s"B Two:\t\t\t$likelihoodTwo\t${likelihoodTwo / likelihoodFair}")

  val uniformPrior = (0 to 49) ++ (51 to 100)
  //val uniformPrior = 0 to 100
  val likelihoodUniform = new OptimisedEuro(ProbabilityMassFunction.fromSeq(uniformPrior)).update(data, normalise = false).total
  println(s"B Uniform:\t\t$likelihoodUniform\t${likelihoodUniform / likelihoodFair}")

  val likelihoodTriangular = triangularSuite.update(data, normalise = false).total
  println(s"B Triangular:\t$likelihoodTriangular\t${likelihoodTriangular / likelihoodFair}")

  private def printResults(pmf: ProbabilityMassFunction[Int]): Unit = {
    println(s"Maximum Likelihood: ${pmf.maximumLikelihood}")
    println(s"Mean: ${pmf.mean}")
    println(s"Median: ${pmf.percentile(50)}")
    println(s"CI: ${pmf.toCdf.credibleInterval(90.0)}")
  }

  private def trianglePrior: ProbabilityMassFunction[Int] = {
    val left = (0 to 50).foldLeft(new ProbabilityMassFunction[Int]) {
      case (result, x) => result.set(x, x)
    }

    (51 to 100).foldLeft(left) {
      case (result, x) => result.set(x, 100.0 - x)
    }.normalise
  }
}

class Euro(val prior: ProbabilityMassFunction[Int]) extends BayesianSuite[Int, Char] {
  override def toPmf: ProbabilityMassFunction[Int] = prior

  override def fromPmf(pmf: ProbabilityMassFunction[Int]): TargetType = new Euro(pmf).asInstanceOf[TargetType]

  override def likelihood(data: Char, hypothesis: Int): Double = {
    if (data == 'H') {
      hypothesis.toDouble / 100.0
    } else {
      1.0 - (hypothesis.toDouble / 100.0)
    }
  }
}

class OptimisedEuro(val prior: ProbabilityMassFunction[Int]) extends BayesianSuite[Int, (Int, Int)] {
  override def toPmf: ProbabilityMassFunction[Int] = prior

  override def fromPmf(pmf: ProbabilityMassFunction[Int]): TargetType = new OptimisedEuro(pmf).asInstanceOf[TargetType]

  override def likelihood(data: (Int, Int), hypothesis: Int): Double = {
    val x = hypothesis.toDouble / 100.0
    val (heads, tails) = data
    pow(x, heads) * pow(1.0 - x, tails)
  }
}
