package bayes

import scala.math.{atan2, cos, pow}

object Paintball extends App {
  val alphas = Range.inclusive(0, 30)
  val betas = Range.inclusive(1, 50)
  val locations = Range.inclusive(0, 30)

  val suite = new Paintball(alphas, betas, locations)
    .updateAll(Seq(15, 16, 18, 21))

  val marginalAlpha = suite.marginal(0)
  val marginalBeta = suite.marginal(1)

  println(s"Alpha CI: ${marginalAlpha.toCdf.credibleInterval(50.0)}")
  println(s"Beta CI : ${marginalBeta.toCdf.credibleInterval(50.0)}")

  //suite.conditional(0, 1, 10).items.foreach(e => println(s"${e._1},${e._2}"))
  //suite.conditional(0, 1, 20).items.foreach(e => println(s"${e._1},${e._2}"))
  //suite.conditional(0, 1, 40).items.foreach(e => println(s"${e._1},${e._2}"))

  val plot = crediblePlot(suite)
  for (a <- alphas) {
    println(a + ": " + betas.map(b => plot.getOrElse((a, b), 0)).mkString(","))
  }

  private def crediblePlot(suite: Paintball): Map[(Int, Int), Int] = {
    Seq(75.0, 50.0, 25.0)
      .flatMap(percentage => suite.maximumLikelihoodInterval(percentage))
      .groupBy(x => x)
      .mapValues(_.size)
  }
}

class Paintball(private val pmf: ProbabilityMassFunction[(Int, Int)],
                private val locations: Seq[Int])
  extends BayesianSuite[(Int, Int), Int] with Joint2[Int] {

  def this(alphas: Seq[Int], betas: Seq[Int], locations: Seq[Int]) = {
    this(ProbabilityMassFunction.fromSeq(
      for (a <- alphas; b <- betas) yield (a, b)),
      locations)
  }

  override def toPmf: ProbabilityMassFunction[(Int, Int)] = pmf

  override def fromPmf(pmf: ProbabilityMassFunction[(Int, Int)]): TargetType = new Paintball(pmf, locations).asInstanceOf[TargetType]

  override def likelihood(location: Int, hypothesis: (Int, Int)): Double = {
    val (alpha, beta) = hypothesis
    val pmf = locationPmf(alpha, beta, locations)
    pmf.probability(location)
  }

  private def locationPmf(alpha: Int, beta: Int, locations: Seq[Int]): ProbabilityMassFunction[Int] = {
    locations.foldLeft(new ProbabilityMassFunction[Int]) {
      case (resultPmf, location) =>
        val probability = 1.0 / strafingSpeed(alpha, beta, location)
        resultPmf.set(location, probability)
    }.normalise
  }

  private def strafingSpeed(alpha: Int, beta: Int, location: Int): Double = {
    val theta = atan2((location - alpha).toDouble, beta.toDouble)
    beta.toDouble / pow(cos(theta), 2.0)
  }
}
