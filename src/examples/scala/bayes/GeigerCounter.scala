package bayes

import bayes.pdf.PoissonProbabilityDensityFunction.poissonPmf
import bayes.util.Stats

object GeigerCounter extends App {

  val f = 0.1
  val k = 15

  /*Seq(100, 250, 400).foreach { r =>
    val suite = new Detector(r, f).update(k)
    println(suite.maximumLikelihood)
    //suite.items.foreach(i => println(s"${i._1},${i._2}"))
  }*/

  val emitter = new Emitter(1 to 500, f).update(k)
  //emitter.posterierDistributionR.items.foreach(i => println(s"${i._1},${i._2}"))
  emitter.posterierDistributionN.items.foreach(i => println(s"${i._1},${i._2}"))

  class Emitter(private val pmf: ProbabilityMassFunction[Detector]) extends BayesianSuite[Detector, Int] {
    override def toPmf: ProbabilityMassFunction[Detector] = pmf
    override def fromPmf(pmf: ProbabilityMassFunction[Detector]): TargetType = new Emitter(pmf).asInstanceOf[TargetType]

    def this(rates: Seq[Int], f: Double) = this(ProbabilityMassFunction.fromSeq(rates.map(r => new Detector(r, f))))

    override def likelihood(data: Int, hypothesis: Detector): Double = hypothesis.suiteLikelihood(data)

    override def update(data: Int, normalise: Boolean): TargetType = {
      val newEmitter = super.update(data, normalise)
      fromPmf(new ProbabilityMassFunction[Detector](newEmitter.items.map { case (d, p) => (d.update(data, normalise), p) }))
    }

    def posterierDistributionR: ProbabilityMassFunction[Int] =
      new ProbabilityMassFunction(pmf.items.map { case (d, p) => (d.r, p) })

    def posterierDistributionN: ProbabilityMassFunction[Int] =
      ProbabilityMassFunction.makeMixture[Int](new ProbabilityMassFunction(pmf.items.map(i => (i._1.toPmf, i._2))))
  }

  class Detector private (
      private val pmf: ProbabilityMassFunction[Int],
      val r: Int,
      val f: Double) extends BayesianSuite[Int, Int] {

    override def toPmf: ProbabilityMassFunction[Int] = pmf
    override def fromPmf(pmf: ProbabilityMassFunction[Int]): TargetType = new Detector(pmf, r, f).asInstanceOf[TargetType]

    def this(r: Int, f: Double, high: Int = 500) = this(poissonPmf(r, low = 0, high), r, f)

    override def likelihood(data: Int, hypothesis: Int): Double = {
      val k = data
      val n = hypothesis
      val probability = f

      val result = Stats.Binomial.pmf(k, n, probability)
      result
    }

    def suiteLikelihood(data: Int): Double = {
      pmf.items.foldLeft(0.0) {
        case (result, (hypothesis, probability)) =>
          val like = likelihood(data, hypothesis)
          result + (like * probability)
      }
    }
  }

}
