package bayes

import bayes.ProbabilityMassFunction.makeMixture
import bayes.pdf.{PoissonProbabilityDensityFunction, EstimatedProbabilityDensityFunction}
import bayes.util.Stats.linspace

object RedLine extends App {

  //longest hypothetical time between trains, in seconds
  val UPPER_BOUND = 1200.0

  /* observed gaps between trains, in seconds
    collected using code in redline_data.py, run daily 4-6pm
    for 5 days, Monday 6 May 2013 to Friday 10 May 2013*/
  val OBSERVED_GAP_TIMES = Seq(
    428.0, 705.0, 407.0, 465.0, 433.0, 425.0, 204.0, 506.0, 143.0, 351.0,
    450.0, 598.0, 464.0, 749.0, 341.0, 586.0, 754.0, 256.0, 378.0, 435.0,
    176.0, 405.0, 360.0, 519.0, 648.0, 374.0, 483.0, 537.0, 578.0, 534.0,
    577.0, 619.0, 538.0, 331.0, 186.0, 629.0, 193.0, 360.0, 660.0, 484.0,
    512.0, 315.0, 457.0, 404.0, 740.0, 388.0, 357.0, 485.0, 567.0, 160.0,
    428.0, 387.0, 901.0, 187.0, 622.0, 616.0, 585.0, 474.0, 442.0, 499.0,
    437.0, 620.0, 351.0, 286.0, 373.0, 232.0, 393.0, 745.0, 636.0, 758.0
  )

  val PASSENGER_DATA = Seq(
    //k1, y, k2   k1 = number of waiting px, y is wait time for next train, k2 number of arrivals while waiting
    (17, 4.6, 9),
    (22, 1.0, 0),
    (23, 1.4, 4),
    (18, 5.4, 12),
    (4, 5.8, 11)
  )

  val cdfZ_ = ProbabilityMassFunction.fromSeq(OBSERVED_GAP_TIMES).toCdf
  val sampleZ_ = cdfZ_.sample(220)
  val pdfZ_ = new EstimatedProbabilityDensityFunction[Double](sampleZ_)
  val pmfZ_ = pdfZ_.toPmf(Range.inclusive(60, UPPER_BOUND.toInt, 10).map(_.toDouble))

  val n = 220
  val cdfZ = ProbabilityMassFunction.fromSeq(OBSERVED_GAP_TIMES).toCdf
  val sampleZ = cdfZ.sample(n)
  val observedPmfZ = ProbabilityMassFunction.fromSeq(sampleZ)
  val cdfZb = observedPmfZ.bias.toCdf
  val sampleZb = cdfZb.sample(n) ++ Seq(1800.0, 2400.0, 3000.0)
  val pdfZb = new EstimatedProbabilityDensityFunction[Double](sampleZb)
  val pmfZb = pdfZb.toPmf(Range.inclusive(60, 3000, 10).map(_.toDouble))
  val pmfZ = pmfZb.unbias


  val wtc = WaitTimeCalculation(pmfZ)

  /*val arrivalRateEstimation = ArrivalRateEstimation(PASSENGER_DATA)
  val waitTimeEstimation = WaitTimeEstimation(wtc, arrivalRateEstimation, 15)
  waitTimeEstimation.mixture.toCdf.items.foreach(x => println(s"${x._1},${x._2}"))*/

  Range.inclusive(0, 35).foreach(x => println(s"$x,${probabilityOfLongWait(x, 15)}"))

  def probabilityOfLongWait(numberOfObservedPassengers: Int, waitMinutes: Int): Double = {
    val ete = new ElapsedTimeEstimation(wtc, 2.0 / 60.0, numberOfObservedPassengers)
    val cdfY = ete.pmfY.toCdf
    1.0 - cdfY.probability(waitMinutes * 60.0)
  }

  case class WaitTimeEstimation(wtc: WaitTimeCalculation, are: ArrivalRateEstimation, numberOfObservedPassenger: Int) {
    val metaPmf: ProbabilityMassFunction[ProbabilityMassFunction[Double]] =
      are.posteriorArrivalRates.items.foldLeft(new ProbabilityMassFunction[ProbabilityMassFunction[Double]]) {
        case (result, (arrivalRate, arrivalRateProbability)) =>
          val ete = ElapsedTimeEstimation(wtc, arrivalRate, numberOfObservedPassenger)
          result.set(ete.pmfY, arrivalRateProbability)
      }

    val mixture: ProbabilityMassFunction[Double] = makeMixture(metaPmf)
  }

  case class ArrivalRateEstimation(passengerData: Seq[(Int, Double, Int)]) {
    val hypotheses = linspace(0.0, 5.0, 51).map(_ / 60.0)
    private val arrivalRate = new ArrivalRate(hypotheses)
    val priorArrivalRates = arrivalRate.toPmf
    val posteriorArrivalRates = passengerData.foldLeft(arrivalRate) {
      case (result, (k1, y, k2)) =>
        result.update((y * 60, k2))
    }.toPmf
  }

  class ArrivalRate(private val pmf: ProbabilityMassFunction[Double])
    extends BayesianSuite[Double, (Double, Int)] {

    def this(arrivalRates: Seq[Double]) =
      this(new ProbabilityMassFunction[Double](arrivalRates.map(h => (h, 1.0))).normalise)

    override def toPmf: ProbabilityMassFunction[Double] = pmf

    override def fromPmf(pmf: ProbabilityMassFunction[Double]): TargetType =
      new ArrivalRate(pmf).asInstanceOf[TargetType]

    override def likelihood(data: (Double, Int), arrivalRatePerSecond: Double): Double = {
      val (waitTime, arrivals) = data
      new PoissonProbabilityDensityFunction(arrivalRatePerSecond * waitTime).density(arrivals)
    }
  }

  case class ElapsedTimeEstimation(wtc: WaitTimeCalculation, passengerArrivalRatePerSecond: Double, numberWaitingPassengers: Int) {
    val priorX = new Elapsed(wtc.pmfX)
    val postX = priorX.update((passengerArrivalRatePerSecond, numberWaitingPassengers)).toPmf
    val pmfY = predictWaitTime(wtc.pmfZb, postX)

    def predictWaitTime(pmfZb: ProbabilityMassFunction[Double], pmfX: ProbabilityMassFunction[Double]): ProbabilityMassFunction[Double] = {
      val pmfY = pmfZb - pmfX
      new ProbabilityMassFunction[Double](pmfY.items.filter(_._1 >= 0).sortBy(_._1)).normalise
    }
  }

  class Elapsed(prior: ProbabilityMassFunction[Double]) extends BayesianSuite[Double, (Double, Int)] {
    override def toPmf: ProbabilityMassFunction[Double] = prior

    override def fromPmf(pmf: ProbabilityMassFunction[Double]): TargetType = new Elapsed(pmf).asInstanceOf[TargetType]

    override def likelihood(data: (Double, Int), secondsSinceLastTrain: Double): Double = {
      val (passengerArrivalRatePerSecond, numberOfPassenger) = data
      new PoissonProbabilityDensityFunction(passengerArrivalRatePerSecond * secondsSinceLastTrain).density(numberOfPassenger)
    }
  }

  case class WaitTimeCalculation(pmfZ: ProbabilityMassFunction[Double]) {
    val pmfZb = pmfZ.bias
    val pmfY = waitTimePmf(pmfZb)
    val pmfX = pmfY
  }

  def waitTimePmf(pmfZb: ProbabilityMassFunction[Double]): ProbabilityMassFunction[Double] = {
    val meta = pmfZb.items.foldLeft(new ProbabilityMassFunction[ProbabilityMassFunction[Double]]) {
      case (result, (gap, probability)) =>
        val uniform = makeUniform(0, gap)
        result.set(uniform, probability)
    }

    makeMixture(meta)
  }

  def makeUniform(low: Double, high: Double): ProbabilityMassFunction[Double] = {
    val range = Range.inclusive(low.toInt, high.toInt, 10)
    range.foldLeft(new ProbabilityMassFunction[Double]) {
      case (result, value) => result.set(value, 1)
    }.normalise
  }
}
