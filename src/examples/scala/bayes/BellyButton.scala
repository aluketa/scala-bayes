package bayes

import bayes.util.RichRandom._
import bayes.util.Stats.Binomial.binomialCoefficient

import scala.math.{exp, log, pow}
import scala.util.Random

object BellyButton extends App {

  /*val d = new Dirichlet(3).update(Seq(3, 2, 1))
  (0 to 2).foreach(i => println(d.marginalBeta(i).mean))*/

  //val species = new Species(3 to 30).update(Seq(3.0, 2.0, 1.0))
  //val pmf = species.toPmf

  val species = new Species2(3 to 30).update(Seq(3, 2, 1))
  println()
}


class Species4(ns: Seq[Int]) extends Species(ns) {
  def updateSingle(data: Seq[Double], normalise: Boolean): Species = {
    data.indices.foldLeft(this) {
      case (result, i) =>
        val one = Seq.fill(i)(0.0) :+ data(i)
        result.update(one)
    }
  }

  override def likelihood(data: Seq[Double], hypothesis: Dirichlet): Double = {
    val like = (0 to 1000).map(ignored => hypothesis.likelihood(data)).sum
    val numUnseen = 1.0 // hypothesis.params.length - data.length + 1

    like * numUnseen * binomialCoefficient(hypothesis.params.length, data.length)
  }
}

class Species2(val hypotheses: Seq[Int], val probabilities: Seq[Double], val params: Seq[Int]) {

  def this(hypotheses: Seq[Int]) = this(hypotheses, Seq.fill(hypotheses.size)(1.0), Seq.fill(hypotheses.last)(1))

  def update(data: Seq[Int]): Species2 = {
    val like = (1 to 1000).foldLeft(Seq.fill(hypotheses.length)(1.0)) {
      case (result, _) => result.zip(sampleLikelihood(data)).map { case (x, y) => x + y }
    }

    val updatedProbabilities = probabilities.zip(like).map { case (x, y) => x * y }
    val normalisedProbabilities = updatedProbabilities.map(x => x / updatedProbabilities.sum)

    val updatedParams = params.padTo(data.length, 0).zip(data.padTo(params.length, 0)).map { case (x, y) => x + y }

    new Species2(hypotheses, normalisedProbabilities, updatedParams)
  }

  private def sampleLikelihood(data: Seq[Int])(implicit rnd: Random = new Random): Seq[Double] = {
    val gammas = params.map(p => rnd.gammaVariate(p, 1.0))
    val row = gammas.take(data.length)
    val col = gammas.foldLeft(Seq[Double]()) { case (result, x) => result :+ (x + result.lastOption.getOrElse(0.0)) }

    val logLikes = hypotheses.foldLeft(Seq[Double]()) {
      case (result, h) =>
        val ps = row.map(d => d / col(h -1))
        val terms = data.zip(ps).map { case (d, p) => d * log(p)}

        result :+ terms.sum
    }

    val zeroedLogLikes = logLikes.map(l => l - logLikes.max)
    val likes = zeroedLogLikes.map(l => exp(l))

    val coefs = hypotheses.map(h => binomialCoefficient(h, data.length))

    likes.zip(coefs).map { case (l, c) => l * c }
  }
}

class Species private (
    private val pmf: ProbabilityMassFunction[Dirichlet]) extends BayesianSuite[Dirichlet, Seq[Double]] {

  def this(ns: Seq[Int]) = this(ProbabilityMassFunction.fromSeq(ns.map(n => new Dirichlet(n))))
  override def toPmf: ProbabilityMassFunction[Dirichlet] = pmf
  override def fromPmf(pmf: ProbabilityMassFunction[Dirichlet]): TargetType = new Species(pmf).asInstanceOf[TargetType]


  override def update(data: Seq[Double], normalise: Boolean): TargetType = {
    val updated = super.update(data, normalise)
    val updatedPmf =
      new ProbabilityMassFunction(
        updated.items.map { case (dirichlet, probability) => (dirichlet.update(data), probability)})
    fromPmf(updatedPmf)
  }

  override def likelihood(data: Seq[Double], hypothesis: Dirichlet): Double = {
    val rnd = new Random
    val like = (1 to 1000).map(ignored => hypothesis.likelihood(data)(rnd)).sum
    like * binomialCoefficient(hypothesis.params.length, data.length)
  }

}

class Dirichlet private(val params: Seq[Double]) {
  def this(n: Int) = this(Seq.fill(n)(1.0))

  def update(data: Seq[Double]): Dirichlet =
    new Dirichlet(params.padTo(data.length, 0.0).zip(data.padTo(params.length, 0.0)).map { case (x, y) => x + y })

  def marginalBeta(i: Int): BetaDistribution[Double] = {
    val alpha0 = params.sum
    val alpha = params(i)
    new BetaDistribution[Double](alpha, alpha0 - alpha)
  }

  def likelihood(data: Seq[Double])(implicit rnd: Random = new Random): Double = {
    if (params.length < data.length) {
      0.0
    } else {
      random(rnd).take(data.length).zip(data).map { case(x, y) => pow(x, y) }.product
    }
  }

  def random(implicit rnd: Random = new Random): Seq[Double] = {
    val p = params.map(p => rnd.gammaVariate(p, 1.0))
    p.map(x => x / p.sum)
  }

  override def toString: String = params.toString()
}
