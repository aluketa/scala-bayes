package bayes

import bayes.pdf.{EstimatedProbabilityDensityFunction, GaussianProbabilityDensityFunction}
import bayes.util.Stats.std

import scala.io.Source

object Showcase extends App {
  val showCases2011 = DataFile("/showcases.2011.csv")
  val showCases2012 = DataFile("/showcases.2012.csv")

  val player1Prices = showCases2011.showCaseOne ++ showCases2012.showCaseOne
  val player2Prices = showCases2011.showCaseTwo ++ showCases2012.showCaseTwo
  val player1Bids = showCases2011.bidOne ++ showCases2012.bidOne
  val player2Bids = showCases2011.bidTwo ++ showCases2012.bidTwo
  val player1Differences = showCases2011.differenceOne ++ showCases2012.differenceOne
  val player2Differences = showCases2011.differenceTwo ++ showCases2012.differenceTwo

  val player1 = Player(player1Prices, player1Bids, player1Differences)
  val player2 = Player(player2Prices, player2Bids, player2Differences)
  val prior = player1.pricePmf

  val posterier = player1.makeBeliefs(20000)
  println(posterier.mean)

  println(player1.optimalBid(20000, player2))
  println(player2.optimalBid(40000, player1))

  player1.printBidGain(20000, player2)
  player2.printBidGain(40000, player1)
}

class GainCalculator(player: Player, opponent: Player, posterier: ProbabilityMassFunction[Int]) {

  def expectedGain(low: Int = 0, high: Int = 75000, step: Int = 100): Seq[(Int, Double)] = {
    val bids = Range(low, high, step)
    bids.map(b => (b, expectedGain(b)))
  }

  private def expectedGain(bid: Int): Double = {
    posterier.items.foldLeft(0.0) { case (result, (price, probability)) =>
      result + (probability * gain(bid, price))
    }
  }

  private def gain(bid: Int, price: Int): Double = {
    if (bid > price)
      0.0
    else {
      val difference = price - bid
      if (difference <= 250)
        2 * price * probabilityOfWinning(difference)
      else
        price * probabilityOfWinning(difference)
    }
  }

  private def probabilityOfWinning(difference: Int): Double =
    opponent.probabilityOverbid + opponent.probabilityWorseThan(difference)
}

class Price(priors: ProbabilityMassFunction[Int], player: Player) extends BayesianSuite[Int, Int] {

  override def toPmf: ProbabilityMassFunction[Int] = priors

  override def fromPmf(pmf: ProbabilityMassFunction[Int]): TargetType = new Price(pmf, player).asInstanceOf[TargetType]

  override def likelihood(data: Int, hypothesis: Int): Double = {
    val price = hypothesis
    val guess = data
    val error = price - guess

    player.errorDensity(error)
  }
}

case class Player(prices: Seq[Int], bids: Seq[Int], differences: Seq[Int]) {
  private val pricePdf = new EstimatedProbabilityDensityFunction[Int](prices)
  private val differenceCdf = ProbabilityMassFunction.fromSeq(differences.sorted).toCdf
  private val errorPdf = new GaussianProbabilityDensityFunction[Int](mean = 0, std(differences))

  def errorDensity(error: Int): Double = errorPdf.density(error)

  def makeBeliefs(guess: Int): ProbabilityMassFunction[Int] = {
    new Price(pricePmf, this).update(guess).toPmf
  }

  def pricePmf: ProbabilityMassFunction[Int] = {
    val discretePrices = Range(0, 75000, 100)
    pricePdf.toPmf(discretePrices)
  }

  def probabilityOverbid: Double = differenceCdf.probability(-1)

  def probabilityWorseThan(difference: Int): Double = 1.0 - differenceCdf.probability(difference)

  def optimalBid(guess: Int, opponent: Player): (Int, Double) = {
    val gainCalculator = new GainCalculator(this, opponent, makeBeliefs(guess))
    gainCalculator.expectedGain().foldLeft(0, 0.0) { case ((resultBid, resultGain), (bid, gain)) =>
      if (gain > resultGain) (bid, gain) else (resultBid, resultGain)
    }
  }

  def printBidGain(guess: Int, opponent: Player): Unit = {
    val gainCalculator = new GainCalculator(this, opponent, makeBeliefs(guess))
    gainCalculator.expectedGain().foreach(e => println(s"${e._1},${e._2}"))
  }
}

case class DataFile(filename: String) {
  private val lines = Source.fromURL(getClass.getResource(filename)).getLines().toSeq

  val showCaseOne: Seq[Int] = lines(3).split(',').toSeq.drop(1).map(Integer.parseInt)
  val showCaseTwo: Seq[Int] = lines(4).split(',').toSeq.drop(1).map(Integer.parseInt)
  val bidOne: Seq[Int] = lines(6).split(',').toSeq.drop(1).map(Integer.parseInt)
  val bidTwo: Seq[Int] = lines(7).split(',').toSeq.drop(1).map(Integer.parseInt)
  val differenceOne: Seq[Int] = lines(9).split(',').toSeq.drop(1).map(Integer.parseInt)
  val differenceTwo: Seq[Int] = lines(10).split(',').toSeq.drop(1).map(Integer.parseInt)
}
