package bayes

import bayes.ProbabilityMassFunction.makeMixture
import bayes.pdf.GaussianProbabilityDensityFunction.gaussianPmf
import bayes.util.Stats.Binomial
import bayes.util.{Interpolator, Stats}

import scala.io.Source
import scala.math.{exp, round}

object Sat extends App {

  val scale = readScale()
  val score = readRanks
  val scorePmf = new ProbabilityMassFunction[Double](score).normalise
  val rawPmf = reverseScale(scorePmf, scale)
  val maxValue = rawPmf.values.max
  val prior = divideValues(rawPmf, maxValue)

  val alice = new Sat(prior, scale, maxValue.toInt).update(780)
  val bob = new Sat(prior, scale, maxValue.toInt).update(740)

  val equalTo = alice.probabilityEqual(bob)
  val aliceGreater = alice.probabilityGreaterThan(bob) + (equalTo / 2.0)
  val bobGreater = bob.probabilityGreaterThan(alice) + (equalTo / 2.0)

  val overview = topLevel(alice, bob)
  //println(overview + s" ratio: ${overview.probability("Alice") / overview.probability("Bob")}")

  /*for(efficacy <- Seq(3.0, 1.5, 0.0, -1.5, -3.0); difficulty <- Seq(-1.85, -0.05, 1.75))
   println(s"${probabilityCorrect(efficacy, difficulty)}\t")*/

  val efficacies = gaussianPmf(0.0, 1.5, 3)
  val difficulties = makeDifficulties(-0.05, 1.8, 54)
  val model = rawScoreDistribution(efficacies, difficulties)

  //rawPmf.toCdf.items.foreach(i => println(s"${i._1},${i._2}"))
  //model.toCdf.items.foreach(i => println(s"${i._1},${i._2}"))

  val aliceEfficacy = new Sat2(efficacies, scale, difficulties).update(780)
  val bobEfficacy = new Sat2(efficacies, scale, difficulties).update(740)

  //aliceEfficacy.toCdf.items.foreach(i => println(s"${i._1},${i._2}"))
  //bobEfficacy.toCdf.items.foreach(i => println(s"${i._1},${i._2}"))

  val efficacyOverview = topLevel(aliceEfficacy, bobEfficacy)
  //println(efficacyOverview + s" ratio: ${efficacyOverview.probability("Alice") / efficacyOverview.probability("Bob")}")

  val aliceNextTestPrediction = rawScoreDistribution(aliceEfficacy, difficulties)
  val bobNextTestPrediction = rawScoreDistribution(bobEfficacy, difficulties)

  val aliceBetterInNextTest = aliceNextTestPrediction.probabilityGreaterThan(bobNextTestPrediction)
  val bobBetterInNextTest = bobNextTestPrediction.probabilityGreaterThan(aliceNextTestPrediction)
  val bothEqual = aliceNextTestPrediction.probabilityEqual(bobNextTestPrediction)

  println(s"Alice better: $aliceBetterInNextTest, Bob better: $bobBetterInNextTest, Equal: $bothEqual")
  val joint = Joint.from(alice, bob)
  val heatData = joint.targetPmf.items
    .filter { case ((v1, v2), p) => v1 >= 0.8 && v2 >= 0.8 }
    .map { case ((v1, v2), p) => ((round(v1 * 100).toDouble / 100.0, round(v2 * 100).toDouble / 100.0), p) }
    .toMap

  val keys = heatData.keys.map(_._1).toSeq.distinct.sorted

  for (y <- keys.reverse) {
    println(y + ":" + keys.map(x => heatData((x, y))).mkString(","))
  }
  println(" :" + keys.mkString(","))

  class Sat2(
    prior: ProbabilityMassFunction[Double],
    scale: Interpolator[Double],
    difficulties: Seq[Double]) extends BayesianSuite[Double, Int] {

    override def toPmf: ProbabilityMassFunction[Double] = prior
    override def fromPmf(pmf: ProbabilityMassFunction[Double]): TargetType = new Sat2(pmf, scale, difficulties).asInstanceOf[TargetType]

    override def likelihood(data: Int, hypothesis: Double): Double = {
      val efficacy = hypothesis
      val score = data
      val raw = scale.reverse(score)

      val pmf = pmfCorrect(efficacy, difficulties)
      pmf.probability(raw)
    }
  }

  def makeDifficulties(centre: Double, width: Double, n: Int = 54): Seq[Double] = {
    val (low, high) = (centre - width, centre + width)
    Stats.linspace(low, high, n)
  }

  def rawScoreDistribution(efficacies: ProbabilityMassFunction[Double], difficulties: Seq[Double]): ProbabilityMassFunction[Double] = {
    val pmfs = efficacies.items.foldLeft(new ProbabilityMassFunction[ProbabilityMassFunction[Double]]) {
      case (result, (efficacy, probability)) => result.set(pmfCorrect(efficacy, difficulties), probability)
    }

    makeMixture(pmfs)
  }

  def pmfCorrect(efficacy: Double, difficulties: Seq[Double]): ProbabilityMassFunction[Double] = {
    val probabilities = difficulties.map(difficulty => probabilityCorrect(efficacy, difficulty))
    val pmfs: Seq[ProbabilityMassFunction[Double]] = probabilities.map(probability => binaryPmf(probability))
    val result = pmfs.reduceLeft((result, pmf) => result.add(pmf))
    new ProbabilityMassFunction[Double](result.items.sortBy(_._1))
  }

  def probabilityCorrect(efficacy: Double, difficulty: Double, a: Double = 1.0): Double =
    1.0 / (1.0 + exp(-a * (efficacy - difficulty)))

  def binaryPmf(probability: Double): ProbabilityMassFunction[Double] = {
    new ProbabilityMassFunction[Double]
      .set(1.0, probability)
      .set(0.0, 1.0 - probability)
  }

  def topLevel(alice: ProbabilityMassFunction[Double], bob: ProbabilityMassFunction[Double]): ProbabilityMassFunction[String] = {
    val equalTo = alice.probabilityEqual(bob)
    val aliceGreater = alice.probabilityGreaterThan(bob) + (equalTo / 2.0)
    val bobGreater = bob.probabilityGreaterThan(alice) + (equalTo / 2.0)

    new ProbabilityMassFunction[String]
      .set("Alice", aliceGreater)
      .set("Bob", bobGreater)
      .normalise
  }

  class Sat(
    prior: ProbabilityMassFunction[Double],
    scale: Interpolator[Double],
    maxScore: Int) extends BayesianSuite[Double, Int] {

    override def toPmf: ProbabilityMassFunction[Double] = prior

    override def fromPmf(pmf: ProbabilityMassFunction[Double]): TargetType =
      new Sat(pmf, scale, maxScore).asInstanceOf[TargetType]

    override def likelihood(data: Int, hypothesis: Double): Double = {
      val probabilityCorrect = hypothesis
      val score = data

      val k = scale.reverse(score).toInt
      val n = maxScore
      Binomial.pmf(k, n, probabilityCorrect)
    }
  }

  private def divideValues(pmf: ProbabilityMassFunction[Double], maxValue: Double): ProbabilityMassFunction[Double] = {
    pmf.items.foldLeft(new ProbabilityMassFunction[Double]) {
      case (result, (value, probability)) => result.set(value / maxValue, probability)
    }
  }

  private def reverseScale(
    scorePmf: ProbabilityMassFunction[Double],
    scale: Interpolator[Double]): ProbabilityMassFunction[Double] = {

    scorePmf.items.foldLeft(new ProbabilityMassFunction[Double]) {
      case (pmf, (value, probability)) => pmf.increment(scale.reverse(value), probability)
    }
  }

  private def readScale(col: Int = 2): Interpolator[Double] = {
    def parseRange(range: String): Double = {
      val d = range.replaceAll("\"", "").split("-").map(_.toDouble)
      d.sum / d.length
    }

    val lines = Source.fromURL(getClass.getResource("/sat_scale.csv")).getLines().drop(2).toSeq

    val (rawValues, scores) = lines
      .map(_.split(","))
      .filter(cols => cols.length > col)
      .map { cols =>
      val score = if (cols.length > col + 1) parseRange(cols(col + 1)) else 0.0
      (cols(col).toDouble, score)
    }.unzip

    new Interpolator[Double](rawValues.sorted, scores.sorted)
  }

  private def readRanks: Seq[(Double, Double)] = {
    val lines = Source.fromURL(getClass.getResource("/sat_ranks.csv"))
      .getLines()
      .drop(3)

    lines
      .map(_.split(",").take(2))
      .map(_.filter(_.length > 0).map(_.toDouble))
      .filter(_.length == 2)
      .map(x => (x(0), x(1)))
      .toSeq
      .sortBy(x => x._1)
  }
}


