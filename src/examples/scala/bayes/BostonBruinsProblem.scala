package bayes

import java.lang.Math.pow

import bayes.ProbabilityMassFunction.{makeMixture, probabilityLessThan}
import bayes.pdf.ExponentialProbabilityDensityFunction.exponentialPmf
import bayes.pdf.GaussianProbabilityDensityFunction.gaussianPmf
import bayes.pdf.PoissonProbabilityDensityFunction
import bayes.pdf.PoissonProbabilityDensityFunction.poissonPmf

object BostonBruinsProblem extends App {
  val suiteBruins = new Hockey("Bruins").updateAll(Seq(0, 2, 8, 4))
  val suiteCanucks = new Hockey("Canucks").updateAll(Seq(1, 3, 1, 0))

  val goalDistributionBruins = goalPmf(suiteBruins, 10)
  val goalDistributionCanucks = goalPmf(suiteCanucks, 10)

  val distributionDifference = goalDistributionBruins - goalDistributionCanucks

  println(s"Prob Bruins Win: ${distributionDifference.probabilityGreaterThan(0)}")
  println(s"Prob Bruins Lose: ${distributionDifference.probabilityLessThan(0)}")
  println(s"Prob Tie: ${distributionDifference.probability(0)}")

  val goalTimeDistributionBruins = goalTimePmf(suiteBruins)
  val goalTimeDistributionCanucks = goalTimePmf(suiteCanucks)

  val probBruinsLose = distributionDifference.probabilityLessThan(0)
  val probTie = distributionDifference.probability(0)
  val probBruinsWinOvertime = probabilityLessThan(goalTimeDistributionBruins, goalTimeDistributionCanucks)
  val probBruinsWin = distributionDifference.probabilityGreaterThan(0) + (probTie * probBruinsWinOvertime)

  val probBruinsWinSeries = pow(probBruinsWin, 2) + (2.0 * probBruinsWin * (1.0 - probBruinsWin) * probBruinsWin)

  println(s"Prob Bruins win next game: $probBruinsWin")
  println(s"Prob Bruins win series: $probBruinsWinSeries")

  def goalPmf(team: Hockey, maxGoals: Int): ProbabilityMassFunction[Int] = {
    val meta = team.items.foldLeft(new ProbabilityMassFunction[ProbabilityMassFunction[Int]]) {
      case (result, (lambda, probability)) =>
        val pmf = poissonPmf(lambda, 0, maxGoals)
        result.set(pmf, probability)
    }

    makeMixture(meta)
  }

  def goalTimePmf(team: Hockey): ProbabilityMassFunction[Double] = {
    val meta = team.items.foldLeft(new ProbabilityMassFunction[ProbabilityMassFunction[Double]]) {
      case (result, (goals, probability)) =>
        val goalTimeDistribution = exponentialPmf(goals, 0, 2, 2001) //high/low is number of games
        result.set(goalTimeDistribution, probability)
    }

    makeMixture(meta)
  }
}

class Hockey(val name: String, private val prior: ProbabilityMassFunction[Double])
  extends BayesianSuite[Double, Int] {

  def this(name: String) = this(name, gaussianPmf(2.7, 0.3, 4))

  override def toPmf: ProbabilityMassFunction[Double] = prior

  override def fromPmf(pmf: ProbabilityMassFunction[Double]): TargetType = new Hockey(name, pmf).asInstanceOf[TargetType]

  override def likelihood(data: Int, hypothesis: Double): Double = {
    new PoissonProbabilityDensityFunction(lambda = hypothesis).density(x = data)
  }
}


