package bayes

import bayes.mutable.{ProbabilityMassFunction => MutableProbabilityMassFunction}
import bayes.util.Stats.Norm

import scala.math._
import scala.util.Random

object Kidney extends App {

  val model: CumulativeDistributionFunction[Double] = {
    val values = Range.Double(-1.5, 6.5, 1.0)
    val probabilities = Seq(0.0, 2.0, 31.0, 42.0, 48.0, 51.0, 52.0, 53.0).map(_ / 53.0)
    new CumulativeDistributionFunction[Double](values, probabilities)
  }

  def rdtSequence(random: Random = new Random): Stream[Double] = model.value(random.nextDouble()) #:: rdtSequence(random)

  def correlatedRdtSequence
  (rho: Double, cdf: CumulativeDistributionFunction[Double], previousValue: Option[Double] = None)
    (implicit rnd: Random = new Random): Stream[Double] = {
    def transform(x: Double): Double = {
      val p = Norm.standardCdf(x)
      cdf.value(p)
    }

    val nextValue = previousValue match {
      case Some(x) =>
        val sigma = sqrt(1 - pow(rho, 2.0))
        Norm.random(x * rho, sigma)(rnd)
      case _ => Norm.random()(rnd)
    }

    transform(nextValue) #:: correlatedRdtSequence(rho, cdf, Some(nextValue))(rnd)
  }

  def makeSequence(
    rdtSequenceGenerator: () => Stream[Double],
    interval: Double,
    initialVolume: Double,
    maxVolume: Double,
    cache: JointAgeSize): Stream[Double] = {
    var age = 0.0
    var result = initialVolume

    rdtSequenceGenerator().map {
      rdt =>
        val doublings = rdt * interval
        result = result * pow(2, doublings)
        age = age + interval
        cache.add(age, result)
        result
    }.takeWhile(_ < maxVolume)
  }

  def volume(diameter: Double, factor: Double = 4.0 * Pi / 3.0): Double = factor * pow(diameter / 2.0, 3.0)

  def percentilesOfAge(rdtSequenceGenerator: () => Stream[Double], cm: Double, percentile: Int): Double = {
    val cache = new JointAgeSize
    (1 to 1000) foreach { n =>
      makeSequence(rdtSequenceGenerator, 0.67, 0.01, volume(20.0), cache).mkString(",")
    }
    cache.conditionalCdf(cm).percentile(percentile)
  }

  /*val cache = new JointAgeSize
  (1 to 1000) foreach { n =>
    makeSequence(rdtSequence(), 0.67, 0.01, volume(20.0), cache).mkString(",")
  }*/

  /*println(percentilesOfAge(() => rdtSequence(), 6.0, 5))
  println(percentilesOfAge(() => rdtSequence(), 6.0, 25))
  println(percentilesOfAge(() => rdtSequence(), 6.0, 50))
  println(percentilesOfAge(() => rdtSequence(), 6.0, 75))
  println(percentilesOfAge(() => rdtSequence(), 6.0, 95))
  println()
  println(percentilesOfAge(() => correlatedRdtSequence(0.4, model), 6.0, 5))
  println(percentilesOfAge(() => correlatedRdtSequence(0.4, model), 6.0, 25))
  println(percentilesOfAge(() => correlatedRdtSequence(0.4, model), 6.0, 50))
  println(percentilesOfAge(() => correlatedRdtSequence(0.4, model), 6.0, 50))
  println(percentilesOfAge(() => correlatedRdtSequence(0.4, model), 6.0, 75))
  println(percentilesOfAge(() => correlatedRdtSequence(0.4, model), 6.0, 95))*/

  val cache = new JointAgeSize
  (1 to 1000) foreach { n =>
    makeSequence(() => correlatedRdtSequence(0.8, model), 0.67, 0.01, volume(20.0), cache).mkString(",")
  }

  println(cache.probabilityOlder(15.5, 8))
}

class JointAgeSize {
  val pmf = new MutableProbabilityMassFunction[(Double, Double)]

  def add(age: Double, volume: Double): Unit = {
    val cm = diameter(volume)
    val bucket = toBucket(cm)
    pmf.increment((age, bucket), 1.0)
  }

  private def diameter(volume: Double, factor: Double = 3.0 / Pi / 4.0, exp: Double = 1.0 / 3.0): Double = {
    2.0 * pow(factor * volume, exp)
  }

  private def toBucket(cm: Double, factor: Double = 10.0): Double = {
    round(factor * log(cm))
  }

  def ages: Seq[Double] = pmf.values.map(_._1).distinct.sorted

  def joint: Joint[Double, (Double, Double)] =
    new ProbabilityMassFunction[(Double, Double)](pmf.normalise().items.sortBy(_._1))

  def conditionalCdf(cm: Double): CumulativeDistributionFunction[Double] =
    joint.conditional(0, 1, toBucket(cm)).toCdf

  def probabilityOlder(cm: Double, age: Double): Double = 1 - conditionalCdf(cm).probability(age)
}

object KidneySimpleModel extends App {
  val interval = 3291.0
  val averageDoublingTime = 811.0 * 3.0

  val doublingsSinceDischarge = interval / averageDoublingTime

  val sizeAtDiagnosis = 15.5
  val sizeAtDischarge = sizeAtDiagnosis / pow(2.0, doublingsSinceDischarge)

  println(s"Interval (days): $interval")
  println(s"Interval (years): ${interval / 365}")
  println(s"Average doubling time: $averageDoublingTime")
  println(s"Doublings since discharge: $doublingsSinceDischarge")
  println(s"Size (diameter) at diagnosis: $sizeAtDiagnosis")
  println(s"Size (diameter) at discharge: $sizeAtDischarge")

  val assumedSizeAtDischarge = 0.1
  val assumedDoublings = log(sizeAtDiagnosis / assumedSizeAtDischarge) / log(2.0)
  val assumedDoublingTime = interval / assumedDoublings

  val volumetricDoublingTime = assumedDoublingTime / 3.0
  val reciprocalDoublingTime = 365.0 / volumetricDoublingTime
  val probability = Kidney.model.probability(reciprocalDoublingTime)

  println(s"Assumed doublings: $assumedDoublings")
  println(s"Assumed doubling time: $assumedDoublingTime")
  println(s"Volumetric Doubling Time: $volumetricDoublingTime")
  println(s"Reciprocal Doubling Time: $reciprocalDoublingTime")
  println(s"Prob{RDT > 2.4} ${1 - probability}")
}
