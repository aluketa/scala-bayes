package bayes.util

import Bisect.bisect
import org.specs2.mutable.Specification

class BisectSpec extends Specification {
  "A bi-section operation" should {
    "return the correct index for the correct value in a single item sequence" >> {
      val data = Seq(5.0)
      bisect(data, 5.0) must beEqualTo(1)
    }

    "return the correct indices for the correct values in a two item seq" >> {
      val data = Seq(5.0, 7.0)
      bisect(data, 5.0) must beEqualTo(1)
      bisect(data, 7.0) must beEqualTo(2)
    }

    "return the correct indices for the correct values in a three item seq" >> {
      val data = Seq(5.0, 7.0, 11.0)
      bisect(data, 5.0) must beEqualTo(1)
      bisect(data, 7.0) must beEqualTo(2)
      bisect(data, 11.0) must beEqualTo(3)
    }

    "return the correct indices for the correct values in a four item seq" >> {
      val data = Seq(5.0, 7.0, 11.0, 13.0)
      bisect(data, 5.0) must beEqualTo(1)
      bisect(data, 7.0) must beEqualTo(2)
      bisect(data, 11.0) must beEqualTo(3)
      bisect(data, 13.0) must beEqualTo(4)
    }

    "return the correct indices for the correct values in a five item seq" >> {
      val data = Seq(5.0, 7.0, 11.0, 13.0, 17.0)
      bisect(data, 5.0) must beEqualTo(1)
      bisect(data, 7.0) must beEqualTo(2)
      bisect(data, 11.0) must beEqualTo(3)
      bisect(data, 13.0) must beEqualTo(4)
      bisect(data, 17.0) must beEqualTo(5)
    }

    "return the correct index for an approximate value in a single item sequence" >> {
      val data = Seq(5.0)
      bisect(data, 4.5) must beEqualTo(0)
      bisect(data, 5.5) must beEqualTo(1)
    }

    "return the correct indices for approximate values in a two item seq" >> {
      val data = Seq(5.0, 7.0)
      bisect(data, 4.5) must beEqualTo(0)
      bisect(data, 5.5) must beEqualTo(1)
      bisect(data, 7.5) must beEqualTo(2)
    }

    "return the correct indices for approximate values in a three item seq" >> {
      val data = Seq(5.0, 7.0, 11.0)
      bisect(data, 4.5) must beEqualTo(0)
      bisect(data, 5.5) must beEqualTo(1)
      bisect(data, 7.5) must beEqualTo(2)
      bisect(data, 12.5) must beEqualTo(3)
    }

    "return the correct indices for approximate values in a four item seq" >> {
      val data = Seq(5.0, 7.0, 11.0, 13.0)
      bisect(data, 4.5) must beEqualTo(0)
      bisect(data, 5.5) must beEqualTo(1)
      bisect(data, 7.5) must beEqualTo(2)
      bisect(data, 12.5) must beEqualTo(3)
      bisect(data, 13.5) must beEqualTo(4)
    }

    "return the correct indices for approximate values in a five item seq" >> {
      val data = Seq(5.0, 7.0, 11.0, 13.0, 17.0)
      bisect(data, 4.5) must beEqualTo(0)
      bisect(data, 5.5) must beEqualTo(1)
      bisect(data, 7.5) must beEqualTo(2)
      bisect(data, 12.5) must beEqualTo(3)
      bisect(data, 13.5) must beEqualTo(4)
      bisect(data, 17.5) must beEqualTo(5)
    }

    "return the correct indices for negative values" >> {
      val data = Seq(-10.0, -5.0, -1.0, 0.0, 5.0, 10.0)
      bisect(data, -1.0) must beEqualTo(3)
    }
  }
}
