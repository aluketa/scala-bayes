package bayes.util

import bayes.util.Stats._
import org.specs2.mutable.Specification

class StatsSpec extends Specification {
  "The stats utility" should {
    val dataSet = Seq(28800, 28868, 28941, 28957, 28958)

    "return a sample covariance value for a given data set" >> {
      sampleCovariance(dataSet) must beCloseTo(4800.7, 0.00001)
    }

    "return the correct standard deviation for a given data set" >> {
      std(dataSet) must beCloseTo(61.97225, 0.00001)
    }

    "return the correct values from the linspace function" >> {
      linspace(1.0, 10.0, 10) must beEqualTo(Seq(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0))
      linspace(2.0, 3.0, 5) must beEqualTo(Seq(2.0, 2.25, 2.5, 2.75, 3.0))
      linspace(10.0, 50.0, 5) must beEqualTo(Seq(10.0, 20.0, 30.0, 40.0, 50.0))
      linspace(0.0, 75000.0, 101) must beEqualTo(Seq(
        0.0, 750.0, 1500.0, 2250.0, 3000.0, 3750.0, 4500.0,
        5250.0, 6000.0, 6750.0, 7500.0, 8250.0, 9000.0, 9750.0,
        10500.0, 11250.0, 12000.0, 12750.0, 13500.0, 14250.0, 15000.0,
        15750.0, 16500.0, 17250.0, 18000.0, 18750.0, 19500.0, 20250.0,
        21000.0, 21750.0, 22500.0, 23250.0, 24000.0, 24750.0, 25500.0,
        26250.0, 27000.0, 27750.0, 28500.0, 29250.0, 30000.0, 30750.0,
        31500.0, 32250.0, 33000.0, 33750.0, 34500.0, 35250.0, 36000.0,
        36750.0, 37500.0, 38250.0, 39000.0, 39750.0, 40500.0, 41250.0,
        42000.0, 42750.0, 43500.0, 44250.0, 45000.0, 45750.0, 46500.0,
        47250.0, 48000.0, 48750.0, 49500.0, 50250.0, 51000.0, 51750.0,
        52500.0, 53250.0, 54000.0, 54750.0, 55500.0, 56250.0, 57000.0,
        57750.0, 58500.0, 59250.0, 60000.0, 60750.0, 61500.0, 62250.0,
        63000.0, 63750.0, 64500.0, 65250.0, 66000.0, 66750.0, 67500.0,
        68250.0, 69000.0, 69750.0, 70500.0, 71250.0, 72000.0, 72750.0,
        73500.0, 74250.0, 75000.0))
    }

    "return the correct values for the error function" >> {
      erf(0.1) must beCloseTo(0.112462916, 0.000001)
      erf(0.2) must beCloseTo(0.222702589, 0.000001)
      erf(0.3) must beCloseTo(0.328626759, 0.000001)
      erf(0.4) must beCloseTo(0.428392355, 0.000001)
      erf(0.5) must beCloseTo(0.520499878, 0.000001)
      erf(0.6) must beCloseTo(0.603856091, 0.000001)
      erf(0.7) must beCloseTo(0.677801194, 0.000001)
      erf(0.8) must beCloseTo(0.742100965, 0.000001)
      erf(0.9) must beCloseTo(0.796908212, 0.000001)
      erf(1.0) must beCloseTo(0.842700793, 0.000001)

      erf(-1.94) must beCloseTo(-0.993922570, 0.000001)
    }

    "return the correct values for the factorial function" >> {
      factorial(2) must beEqualTo(2)
      factorial(5) must beEqualTo(120)
      factorial(7) must beEqualTo(5040)
      factorial(10) must beEqualTo(3628800)
    }

    "return the correct values for the big factorial function" >> {
      bigFactorial(200) must beEqualTo(BigDecimal("7.886578673647905035523632139321840E+374"))
      bigFactorial(250) must beEqualTo(BigDecimal("3.232856260909107732320814552024365E+492"))
      bigFactorial(400) must beEqualTo(BigDecimal("6.403452284662389526234797031950296E+868"))
    }
  }

  "The Normal Distribution object" should {
    "return the correct values for the pdf function" >> {
      Norm.pdf(-2, 0, 1) must beCloseTo(0.053990, 0.000001)
      Norm.pdf(-1, 0, 1) must beCloseTo(0.241970, 0.000001)
      Norm.pdf(0, 0, 1) must beCloseTo(0.398942, 0.000001)
      Norm.pdf(1, 0, 1) must beCloseTo(0.241970, 0.000001)
      Norm.pdf(2, 0, 1) must beCloseTo(0.053991, 0.000001)

      Norm.pdf(-20, 10, 100) must beCloseTo(0.003813, 0.000001)
      Norm.pdf(-10, 10, 100) must beCloseTo(0.0039104, 0.000001)
      Norm.pdf(0, 10, 100) must beCloseTo(0.003969, 0.000001)
      Norm.pdf(10, 10, 100) must beCloseTo(0.003989, 0.000001)
      Norm.pdf(20, 10, 100) must beCloseTo(0.003969, 0.000001)
    }

    "return the correct values for the logpdf function" >> {
      Norm.logpdf(-2, 10.0, 2.0) must beCloseTo(-19.612085, 0.000001)
      Norm.logpdf(-1, 10.0, 2.0) must beCloseTo(-16.737085, 0.000001)
      Norm.logpdf(0, 10.0, 2.0) must beCloseTo(-14.1120857, 0.000001)
      Norm.logpdf(0.5, 10.0, 2.0) must beCloseTo(-12.893335, 0.000001)
      Norm.logpdf(1.0, 10.0, 2.0) must beCloseTo(-11.737085, 0.000001)

      Norm.logpdf(-2, -10.0, 2.0) must beCloseTo(-9.612085, 0.000001)
      Norm.logpdf(-1, -10.0, 2.0) must beCloseTo(-11.737085, 0.000001)
      Norm.logpdf(0, -10.0, 2.0) must beCloseTo(-14.1120857, 0.000001)
      Norm.logpdf(0.5, -10.0, 2.0) must beCloseTo(-15.393335, 0.000001)
      Norm.logpdf(1.0, -10.0, 2.0) must beCloseTo(-16.737085, 0.000001)
    }

    "return the correct values for the standard cdf function" >> {
      Norm.standardCdf(-2) must beCloseTo(0.022750, 0.000001)
      Norm.standardCdf(-1) must beCloseTo(0.158655, 0.000001)
      Norm.standardCdf(0) must beCloseTo(0.5, 0.000001)
      Norm.standardCdf(0.5) must beCloseTo(0.691462, 0.000001)
      Norm.standardCdf(1.0) must beCloseTo(0.841344, 0.000001)
    }

    "return the correct values for the cdf function" >> {
      Norm.cdf(-2) must beCloseTo(0.022750, 0.000001)
      Norm.cdf(-1) must beCloseTo(0.158655, 0.000001)
      Norm.cdf(0) must beCloseTo(0.5, 0.000001)
      Norm.cdf(0.5) must beCloseTo(0.691462, 0.000001)
      Norm.cdf(1.0) must beCloseTo(0.841344, 0.000001)
    }
  }

  "The Binomial Distribution object" should {
    "return the correct values for the pmf function" >> {
      Binomial.pmf(45, 54, 0.5) must beCloseTo(2.9520476396704636e-07, 0.0000000001)
      Binomial.pmf(40, 54, 0.4) must beCloseTo(3.0745514139283067e-07, 0.0000000001)
      Binomial.pmf(30, 54, 0.3) must beCloseTo(5.5327733342280917e-05, 0.0000000001)
      Binomial.pmf(20, 54, 0.2) must beCloseTo(0.0017087883393206871, 0.0000000001)
      Binomial.pmf(15, 200, 0.1) must beCloseTo(0.050129254543213222, 0.0000000001)
    }
  }
}
