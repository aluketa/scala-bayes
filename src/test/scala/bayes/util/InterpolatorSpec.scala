package bayes.util

import org.specs2.mutable.Specification

class InterpolatorSpec extends Specification {
  "The Interpolator class" should {
    val interpolator = new Interpolator[Int](Seq(1, 2, 3, 4, 5), Seq(10, 20, 30, 40, 50))
    val doubleInterpolator = new Interpolator[Double](Seq(1.1, 2.2, 3.3, 4.4, 5.5), Seq(10.10, 20.20, 30.30, 40.40, 50.50))

    "return the correct answer for a simple lookup" >> {
      interpolator.lookup(0) must beEqualTo(10)
      interpolator.lookup(1) must beEqualTo(10)
      interpolator.lookup(2) must beEqualTo(20)
      interpolator.lookup(3) must beEqualTo(30)
      interpolator.lookup(4) must beEqualTo(40)
      interpolator.lookup(5) must beEqualTo(50)
      interpolator.lookup(6) must beEqualTo(50)
    }

    "return the correct answer for double values" >> {
      doubleInterpolator.lookup(0.0) must beEqualTo(10.10)
      doubleInterpolator.lookup(1.1) must beEqualTo(10.10)
      doubleInterpolator.lookup(2.2) must beEqualTo(20.20)
      doubleInterpolator.lookup(3.3) must beEqualTo(30.30)
      doubleInterpolator.lookup(4.4) must beEqualTo(40.40)
      doubleInterpolator.lookup(5.5) must beEqualTo(50.50)
      doubleInterpolator.lookup(6.0) must beEqualTo(50.50)
    }

    "return the correct answer for a simple reverse lookup" >> {
      interpolator.reverse(10) must beEqualTo(1)
      interpolator.reverse(20) must beEqualTo(2)
      interpolator.reverse(30) must beEqualTo(3)
      interpolator.reverse(40) must beEqualTo(4)
      interpolator.reverse(50) must beEqualTo(5)
    }

    "return the correct answer for a simple reverse lookup for double values" >> {
      doubleInterpolator.reverse(10.10) must beEqualTo(1.1)
      doubleInterpolator.reverse(20.20) must beEqualTo(2.2)
      doubleInterpolator.reverse(30.30) must beEqualTo(3.3)
      doubleInterpolator.reverse(40.40) must beEqualTo(4.4)
      doubleInterpolator.reverse(50.50) must beEqualTo(5.5)
    }

    "return the correct answer for an approximate lookup" >> {
      val f = (3.6 - 3.3) / (4.4 - 3.3)
      val expectedResult = 30.30 + ((40.40 - 30.30) * f)
      doubleInterpolator.lookup(3.6) must beCloseTo(expectedResult, 0.000000001)
    }

    "return the correct answer for an approximate reverse lookup" >> {
      val f = (30.6 - 30.30) / (40.40 - 30.30)
      val expectedResult = 3.3 + ((4.4 - 3.3) * f)
      doubleInterpolator.reverse(30.6) must beCloseTo(expectedResult, 0.000000001)
    }
  }
}
