package bayes

import bayes.ProbabilityMassFunction._
import org.specs2.matcher.MatchResult
import org.specs2.mutable.Specification

import scala.math.log
import scala.util.{Random, Try}

class ProbabilityMassFunctionSpec extends Specification {
  "The immutable Probability Mass Function" should {
    "set stores the correct probability" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 0.25)
        .set("Value B", 0.75)

      pmf.probability("Value A") must beEqualTo(0.25)
      pmf.probability("Value B") must beEqualTo(0.75)
      assertProbabilities(pmf, "Value A" -> 0.25, "Value B" -> 0.75)
    }

    "increment adjusts a given probability accordingly" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 0.25)
        .increment("Value A", 0.11)

      pmf.probability("Value A") must beEqualTo(0.25 + 0.11)
    }

    "incrementing a non existent value adds it" >> {
      val pmf = new ProbabilityMassFunction[String]
        .increment("Value A", 2.3)

      pmf.probability("Value A") must beEqualTo(2.3)
    }

    "multiply adjusts a given probability accordingly" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 19)
        .multiply("Value A", 31)

      pmf.probability("Value A") must beEqualTo(19 * 31)
    }

    "total returns the correct value" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 0.11)
        .set("Value B", 0.17)
        .set("Value B", 0.19)
        .set("Value C", 0.31)

      pmf.total must beEqualTo(0.11 + 0.19 + 0.31)
    }

    "correctly normalises the probability values" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 30)
        .set("Value B", 60)
        .set("Value C", 10)

      pmf.total must beEqualTo(100)

      val normalised = pmf.normalise
      assertProbabilities(normalised, "Value A" -> 0.3, "Value B" -> 0.6, "Value C" -> 0.1)
    }

    "throws NoSuchElementException if the given probability does not exist" >> {
      val pmf = new ProbabilityMassFunction[String]

      Try(pmf.probability("Non existent")) must beFailedTry.withThrowable[NoSuchElementException]
    }

    "return all items" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 30)
        .set("Value B", 60)
        .set("Value C", 10)

      val items = pmf.items
      items must haveSize(3)
      items must contain(("Value A", 30.0))
      items must contain(("Value B", 60.0))
      items must contain(("Value C", 10.0))
    }

    "return all values" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 30)
        .set("Value B", 60)
        .set("Value C", 10)

      val values = pmf.values
      values must haveSize(3)
      values must contain("Value A")
      values must contain("Value B")
      values must contain("Value C")
    }

    "return the value with the maximum probability" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("Value A", 30)
        .set("Value B", 60)
        .set("Value C", 10)
        .normalise

      pmf.max must beEqualTo("Value B")
    }

    "return a null value for the maximum of an empty pmf" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.max must beNull
    }

    "return the correct value for a given percentile" >> {
      val pmf = new ProbabilityMassFunction[Int]
        .set(1, 1 / 10.0)
        .set(2, 2 / 10.0)
        .set(3, 3 / 10.0)
        .set(4, 4 / 10.0)
        .set(5, 5 / 10.0)
        .set(6, 6 / 10.0)
        .set(7, 7 / 10.0)
        .set(8, 8 / 10.0)
        .set(9, 9 / 10.0)
        .set(10, 10 / 10.0)
        .normalise

      pmf.percentile(5) must beEqualTo(2)
      pmf.percentile(25) must beEqualTo(5)
      pmf.percentile(30) must beEqualTo(6)
      pmf.percentile(50) must beEqualTo(7)
      pmf.percentile(60) must beEqualTo(8)
      pmf.percentile(75) must beEqualTo(9)
      pmf.percentile(90) must beEqualTo(10)
    }

    "add two similar pmfs together" >> {
      val pmf = createDice(3) + createDice(3)
      assertProbabilities(pmf, 2 -> 1 / 9.0, 3 -> 2 / 9.0, 4 -> 3 / 9.0, 5 -> 2 / 9.0, 6 -> 1 / 9.0)
    }

    "add two pmfs where the first is larger" >> {
      val pmf = createDice(6) + createDice(3)
      assertProbabilities(pmf, 2 -> 1 / 18.0, 3 -> 2 / 18.0, 4 -> 3 / 18.0, 5 -> 3 / 18.0, 6 -> 3 / 18.0, 7 -> 3 / 18.0, 8 -> 2 / 18.0, 9 -> 1 / 18.0)
    }

    "add two pms where the second is larger" >> {
      val pmf = createDice(3) + createDice(6)
      assertProbabilities(pmf, 2 -> 1 / 18.0, 3 -> 2 / 18.0, 4 -> 3 / 18.0, 5 -> 3 / 18.0, 6 -> 3 / 18.0, 7 -> 3 / 18.0, 8 -> 2 / 18.0, 9 -> 1 / 18.0)
    }

    "subtract two similar pmfs" >> {
      val pmf = createDice(3) - createDice(3)
      assertProbabilities(pmf, -2 -> 1 / 9.0, -1 -> 2 / 9.0, 0 -> 3 / 9.0, 1 -> 2 / 9.0, 2 -> 1 / 9.0)
    }

    "select an item at random" >> {
      val pmf = createDice(21)
      pmf.random(rnd = new Random(1)) must beEqualTo(16)
    }

    "throw a NoSuchElementException if random is called on an empty pmf" >> {
      val pmf = new ProbabilityMassFunction[Int]()
      pmf.random must throwA[NoSuchElementException]
    }

    "generate a pmf from a given list of items" >> {
      val items = Seq(1, 1, 1, 2, 2, 3)
      val pmf = fromSeq(items)

      pmf.probability(1) must beEqualTo(0.5)
      pmf.probability(2) must beCloseTo(0.33333, 0.00001)
      pmf.probability(3) must beCloseTo(0.16666, 0.00001)
    }

    "find the max of two pmfs" >> {
      val pmfMax = createDice(6).max(createDice(6))
      pmfMax.probability(1) must beCloseTo(0.02778, 0.00001)
      pmfMax.probability(2) must beCloseTo(0.08334, 0.00001)
      pmfMax.probability(3) must beCloseTo(0.13889, 0.00001)
      pmfMax.probability(4) must beCloseTo(0.19445, 0.00001)
      pmfMax.probability(5) must beCloseTo(0.25000, 0.00001)
      pmfMax.probability(6) must beCloseTo(0.30556, 0.00001)
    }

    "find the max of two pmfs where the first is larger" >> {
      val pmfMax = createDice(6).max(createDice(3))
      pmfMax.probability(1) must beCloseTo(0.05556, 0.00001)
      pmfMax.probability(2) must beCloseTo(0.16666, 0.00001)
      pmfMax.probability(3) must beCloseTo(0.27777, 0.00001)
      pmfMax.probability(4) must beCloseTo(0.16666, 0.00001)
      pmfMax.probability(5) must beCloseTo(0.16666, 0.00001)
      pmfMax.probability(6) must beCloseTo(0.16666, 0.00001)
    }

    "find the max of two pmfs where the second is larger" >> {
      val pmfMax = createDice(3).max(createDice(6))
      pmfMax.probability(1) must beCloseTo(0.05556, 0.00001)
      pmfMax.probability(2) must beCloseTo(0.16666, 0.00001)
      pmfMax.probability(3) must beCloseTo(0.27777, 0.00001)
      pmfMax.probability(4) must beCloseTo(0.16666, 0.00001)
      pmfMax.probability(5) must beCloseTo(0.16666, 0.00001)
      pmfMax.probability(6) must beCloseTo(0.16666, 0.00001)
    }

    "find the max of selections of a pmf" >> {
      val pmfMax = createDice(6).max(2)
      pmfMax.probability(1) must beCloseTo(0.02778, 0.00001)
      pmfMax.probability(2) must beCloseTo(0.08334, 0.00001)
      pmfMax.probability(3) must beCloseTo(0.13889, 0.00001)
      pmfMax.probability(4) must beCloseTo(0.19445, 0.00001)
      pmfMax.probability(5) must beCloseTo(0.25000, 0.00001)
      pmfMax.probability(6) must beCloseTo(0.30556, 0.00001)
    }

    "return the mean value of a given numerical pmf" >> {
      val pmf = new ProbabilityMassFunction[Int](Map(100 -> 6.0, 200 -> 5.0, 300 -> 4.0, 400 -> 3.0, 500 -> 2.0, 600 -> 1.0)).normalise
      pmf.mean must beCloseTo(266.66667, 0.00001)
    }

    "return the probability of a value being greater than a given parameter" >> {
      val pmf = createDice(6)
      pmf.probabilityGreaterThan(1) must beCloseTo(5.0 / 6.0, 0.00001)
      pmf.probabilityGreaterThan(2) must beCloseTo(4.0 / 6.0, 0.00001)
      pmf.probabilityGreaterThan(3) must beCloseTo(3.0 / 6.0, 0.00001)
      pmf.probabilityGreaterThan(4) must beCloseTo(2.0 / 6.0, 0.00001)
      pmf.probabilityGreaterThan(5) must beCloseTo(1.0 / 6.0, 0.00001)
      pmf.probabilityGreaterThan(6) must beCloseTo(0.0 / 6.0, 0.00001)
    }

    "return the probability of a value being less than a given parameter" >> {
      val pmf = createDice(6)
      pmf.probabilityLessThan(1) must beCloseTo(0.0 / 6.0, 0.00001)
      pmf.probabilityLessThan(2) must beCloseTo(1.0 / 6.0, 0.00001)
      pmf.probabilityLessThan(3) must beCloseTo(2.0 / 6.0, 0.00001)
      pmf.probabilityLessThan(4) must beCloseTo(3.0 / 6.0, 0.00001)
      pmf.probabilityLessThan(5) must beCloseTo(4.0 / 6.0, 0.00001)
      pmf.probabilityLessThan(6) must beCloseTo(5.0 / 6.0, 0.00001)
    }

    "return the probability that a value from a pmf is greater than a value from a given pmf" >> {
      val pmf1 = fromSeq[Int](Seq(1, 2, 3, 4))
      val pmf2 = fromSeq[Int](Seq(2, 3, 4, 5))

      pmf1.probabilityGreaterThan(pmf2) must beEqualTo(0.1875)
    }

    "return the probability that a value from a pmf is less than a value from a given pmf" >> {
      val pmf1 = fromSeq[Int](Seq(1, 2, 3, 4))
      val pmf2 = fromSeq[Int](Seq(2, 3, 4, 5))

      pmf1.probabilityLessThan(pmf2) must beEqualTo(0.625)
    }

    "return the probability that a value from a pmf is equal to a value from a given pmf" >> {
      val pmf1 = fromSeq[Int](Seq(1, 2, 3, 4))
      val pmf2 = fromSeq[Int](Seq(2, 3, 4, 5))

      pmf1.probabilityGreaterThan(pmf2) must beEqualTo(0.1875)
    }

    "return a pmf for a random sample sum of dice throws" >> {
      val dice = List(createDice(6), createDice(6), createDice(6))
      val samplePmf = sampleSum(dice, 5000)

      samplePmf.probability(3) must beLessThan(samplePmf.probability(4))
      samplePmf.probability(4) must beLessThan(samplePmf.probability(5))
      samplePmf.probability(5) must beLessThan(samplePmf.probability(6))
      samplePmf.probability(6) must beLessThan(samplePmf.probability(7))
      samplePmf.probability(7) must beLessThan(samplePmf.probability(8))

      samplePmf.probability(13) must beGreaterThan(samplePmf.probability(14))
      samplePmf.probability(14) must beGreaterThan(samplePmf.probability(15))
      samplePmf.probability(15) must beGreaterThan(samplePmf.probability(16))
      samplePmf.probability(16) must beGreaterThan(samplePmf.probability(17))
      samplePmf.probability(17) must beGreaterThan(samplePmf.probability(18))
    }

    "return a pmf for a random sample max of dice throws" >> {
      val dice = List(createDice(6), createDice(6), createDice(6))
      val samplePmf = sampleMax(dice, 5000)

      samplePmf.probability(2) must beLessThan(samplePmf.probability(3))
      samplePmf.probability(3) must beLessThan(samplePmf.probability(4))
      samplePmf.probability(4) must beLessThan(samplePmf.probability(5))
      samplePmf.probability(5) must beLessThan(samplePmf.probability(6))
    }

    "return a pmf that is a mixture of multiple pmfs" >> {
      val metaPmf = new ProbabilityMassFunction[ProbabilityMassFunction[Int]]()
        .set(createDice(4), 2.0)
        .set(createDice(6), 3.0)
        .set(createDice(8), 2.0)
        .set(createDice(12), 1.0)
        .set(createDice(20), 1.0)
        .normalise

      val mixture: ProbabilityMassFunction[Int] = makeMixture(metaPmf)
      mixture.size must beEqualTo(20)
      mixture.probability(1) must beCloseTo(0.15370, 0.00001)
      mixture.probability(2) must beCloseTo(0.15370, 0.00001)
      mixture.probability(3) must beCloseTo(0.15370, 0.00001)
      mixture.probability(4) must beCloseTo(0.15370, 0.00001)

      mixture.probability(5) must beCloseTo(0.09814, 0.00001)
      mixture.probability(6) must beCloseTo(0.09814, 0.00001)

      mixture.probability(7) must beCloseTo(0.04259, 0.00001)
      mixture.probability(8) must beCloseTo(0.04259, 0.00001)

      mixture.probability(9) must beCloseTo(0.01481, 0.00001)
      mixture.probability(10) must beCloseTo(0.01481, 0.00001)
      mixture.probability(11) must beCloseTo(0.01481, 0.00001)
      mixture.probability(12) must beCloseTo(0.01481, 0.00001)

      mixture.probability(13) must beCloseTo(0.00555, 0.00001)
      mixture.probability(14) must beCloseTo(0.00555, 0.00001)
      mixture.probability(15) must beCloseTo(0.00555, 0.00001)
      mixture.probability(16) must beCloseTo(0.00555, 0.00001)
      mixture.probability(17) must beCloseTo(0.00555, 0.00001)
      mixture.probability(18) must beCloseTo(0.00555, 0.00001)
      mixture.probability(19) must beCloseTo(0.00555, 0.00001)
      mixture.probability(20) must beCloseTo(0.00555, 0.00001)
    }

    "ensure that a returned mixture is normalized" >> {
      val metaPmf = new ProbabilityMassFunction[ProbabilityMassFunction[Int]]()
        .set(new ProbabilityMassFunction[Int](Map(1 -> 5.0, 2 -> 5.0, 3 -> 5.0)), 2.0)
        .set(new ProbabilityMassFunction[Int](Map(1 -> 4.0, 2 -> 4.0, 3 -> 4.0)), 3.0)
        .set(new ProbabilityMassFunction[Int](Map(1 -> 3.0, 2 -> 3.0, 3 -> 3.0)), 4.0)

      val mixture: ProbabilityMassFunction[Int] = makeMixture(metaPmf)
      mixture.size must beEqualTo(3)
      mixture.probability(1) must beCloseTo(0.33333, 0.00001)
      mixture.probability(2) must beCloseTo(0.33333, 0.00001)
      mixture.probability(3) must beCloseTo(0.33333, 0.00001)
    }

    "return the probability of one pmf being less than another one" >> {
      val pmf1 = new ProbabilityMassFunction(Map(1 -> 0.2, 2 -> 0.3, 3 -> 0.5))
      val pmf2 = new ProbabilityMassFunction(Map(2 -> 0.2, 3 -> 0.3, 4 -> 0.5))
      val pmf3 = new ProbabilityMassFunction(Map(4 -> 0.2, 5 -> 0.3, 6 -> 0.5))

      probabilityLessThan(pmf1, pmf2) must beCloseTo(
        (0.2 * 0.2) + (0.2 * 0.3) + (0.2 * 0.5) + (0.3 * 0.3) + (0.3 * 0.5) + (0.5 * 0.5), 0.00001)

      probabilityLessThan(pmf1, pmf3) must beEqualTo(1.0)
    }

    "return the probability of one pmf being greater than another one" >> {
      val pmf1 = new ProbabilityMassFunction(Map(1 -> 0.2, 2 -> 0.3, 3 -> 0.5))
      val pmf2 = new ProbabilityMassFunction(Map(2 -> 0.2, 3 -> 0.3, 4 -> 0.5))
      val pmf3 = new ProbabilityMassFunction(Map(4 -> 0.2, 5 -> 0.3, 6 -> 0.5))

      probabilityGreaterThan(pmf2, pmf1) must beCloseTo(
        (0.2 * 0.2) + (0.2 * 0.3) + (0.2 * 0.5) + (0.3 * 0.3) + (0.3 * 0.5) + (0.5 * 0.5), 0.00001)

      probabilityGreaterThan(pmf3, pmf1) must beEqualTo(1.0)
    }

    "returned a biased version of a given pmf" >> {
      val pmf = createDice(6)
      val biasPmf = pmf.bias

      biasPmf.probability(1) must beCloseTo(0.047619048, 0.000000001)
      biasPmf.probability(2) must beCloseTo(0.095238095, 0.000000001)
      biasPmf.probability(3) must beCloseTo(0.142857143, 0.000000001)
      biasPmf.probability(4) must beCloseTo(0.19047619, 0.000000001)
      biasPmf.probability(5) must beCloseTo(0.238095238, 0.000000001)
      biasPmf.probability(6) must beCloseTo(0.285714286, 0.000000001)
    }

    "return an unbiased version of a given pmf" >> {
      val unbiasPmf = createDice(6).bias.unbias

      unbiasPmf.probability(1) must beCloseTo(1 / 6.0, 0.000000001)
      unbiasPmf.probability(2) must beCloseTo(1 / 6.0, 0.000000001)
      unbiasPmf.probability(3) must beCloseTo(1 / 6.0, 0.000000001)
      unbiasPmf.probability(4) must beCloseTo(1 / 6.0, 0.000000001)
      unbiasPmf.probability(5) must beCloseTo(1 / 6.0, 0.000000001)
      unbiasPmf.probability(6) must beCloseTo(1 / 6.0, 0.000000001)
    }

    "return a zero value pmf when a zero value pmf is normalized" >> {
      val pmf = new ProbabilityMassFunction[Int](Map(1 -> 0.0, 2 -> 0.0))
      val normalised = pmf.normalise

      normalised.total must beEqualTo(0)
      normalised.probability(1) must beEqualTo(0)
      normalised.probability(2) must beEqualTo(0)
    }

    "return the correct maximum likelihood interval for a given percentage" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("One", 1)
        .set("Two", 2)
        .set("Three", 3)
        .set("Four", 4)
        .normalise

      pmf.maximumLikelihoodInterval(40.0) must beEqualTo(Seq("Four"))
      pmf.maximumLikelihoodInterval(70.0) must beEqualTo(Seq("Four", "Three"))
      pmf.maximumLikelihoodInterval(90.0) must beEqualTo(Seq("Four", "Three", "Two"))
      pmf.maximumLikelihoodInterval(100.0) must beEqualTo(Seq("Four", "Three", "Two", "One"))
    }

    "return the correct maximum likelihood" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("One", 1)
        .set("Two", 2)
        .set("Three", 3)
        .set("Four", 4)
        .normalise

      pmf.maximumLikelihood must beEqualTo("Four")
    }

    "return the correct maximum likelihood when all probabilities are zero" >> {
      val pmf = new ProbabilityMassFunction[String]
        .set("One", 0)
        .set("Two", 0)
        .set("Three", 0)
        .set("Four", 0)
        .normalise

      pmf.maximumLikelihood must beEqualTo("One")
    }

    "return the correct maximum likelihood for an empty pmf" >> {
      new ProbabilityMassFunction[String].maximumLikelihood must beNull[String]
    }

    "return the correct maximum probability" >> {
      val pmf = new ProbabilityMassFunction[Int](Map(1 -> 0.05, 2 -> 0.04, 3 -> 0.08, 4 -> 0.03))
      pmf.maximumProbability must beCloseTo(0.08, 0.000001)
      pmf.normalise.maximumProbability must beCloseTo(0.4, 0.000001)
    }

    "return a logarithmic pmf based on an existing pmf" >> {
      val pmf = new ProbabilityMassFunction[Int](Map(1 -> 0.05, 2 -> 0.04, 3 -> 0.08, 4 -> 0.03))
      val log = pmf.logarithmic

      val itemMap: Map[Int, Double] = log.items.toMap
      itemMap(1) must beCloseTo(-0.470003629, 0.000000001)
      itemMap(2) must beCloseTo(-0.693147181, 0.000000001)
      itemMap(3) must beCloseTo(-0.000000000, 0.000000001)
      itemMap(4) must beCloseTo(-0.980829253, 0.000000001)

      log.values must beEqualTo(Seq(1, 2, 3, 4))
    }

    "revert a logarithmic pmf back to a linear one" >> {
      val pmf = new ProbabilityMassFunction[Int](Map(1 -> 0.05, 2 -> 0.04, 3 -> 0.08, 4 -> 0.03))
        .normalise
      val result = pmf.logarithmic.exp.normalise

      result.values must beEqualTo(pmf.values)
      result.items.foldLeft(null.asInstanceOf[MatchResult[Double]]) {
        case (_, (value, probability)) =>
          probability must beCloseTo(pmf.probability(value), 0.000001)
      }
    }

    "allow a logarithmic pmf to be incremented" >> {
      val pmf = new ProbabilityMassFunction[Int](Map(1 -> 0.05, 2 -> 0.04, 3 -> 0.08, 4 -> 0.03))

      val updatedPmf = pmf.multiply(2, 0.05).normalise
      val result = pmf.logarithmic.increment(2, log(0.05)).exp.normalise

      result.items.foldLeft(null.asInstanceOf[MatchResult[Double]]) {
        case (_, (value, probability)) =>
          probability must beCloseTo(updatedPmf.probability(value), 0.000001)
      }
    }
  }

  private def createDice(n: Int): ProbabilityMassFunction[Int] =
    Range(1, n + 1).foldLeft(new ProbabilityMassFunction[Int]) { case (result, i) => result.set(i, 1 / n.toDouble)}.normalise

  private def assertProbabilities[T](pmf: ProbabilityMassFunction[T], probabilities: (T, Double)*): MatchResult[_] = {
    probabilities.foreach { case (value, probability) => pmf.probability(value) must beCloseTo(probability, 0.00001)}
    1 must beEqualTo(1)
  }
}
