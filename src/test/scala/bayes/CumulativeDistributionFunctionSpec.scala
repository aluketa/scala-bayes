package bayes

import org.specs2.mutable.Specification

import scala.util.Random

class CumulativeDistributionFunctionSpec extends Specification {
  "The Cumulative Distribution Function" should {
    "be creatable from an existing probability mass function" >> {
      val cdf = CumulativeDistributionFunction[String](Seq("A" -> 5, "B" -> 7, "C" -> 11))

      cdf.probability("A") must beCloseTo(0.2174, 0.0001)
      cdf.probability("B") must beCloseTo(0.5217, 0.0001)
      cdf.probability("C") must beCloseTo(1.0, 0.0001)
    }

    "calculate the correct percentile values" >> {
      val cdf = new ProbabilityMassFunction[Int]
        .set(1, 1 / 10.0)
        .set(2, 2 / 10.0)
        .set(3, 3 / 10.0)
        .set(4, 4 / 10.0)
        .set(5, 5 / 10.0)
        .set(6, 6 / 10.0)
        .set(7, 7 / 10.0)
        .set(8, 8 / 10.0)
        .set(9, 9 / 10.0)
        .set(10, 10 / 10.0)
        .toCdf

      cdf.percentile(5) must beEqualTo(2)
      cdf.percentile(25) must beEqualTo(5)
      cdf.percentile(30) must beEqualTo(6)
      cdf.percentile(50) must beEqualTo(7)
      cdf.percentile(60) must beEqualTo(8)
      cdf.percentile(75) must beEqualTo(9)
      cdf.percentile(90) must beEqualTo(10)
    }

    "return the correct items for a given cdf" >> {
      val cdf = new ProbabilityMassFunction[Int]
        .set(1, 1)
        .set(2, 1)
        .set(3, 1)
        .set(4, 1)
        .normalise
        .toCdf

      val items = cdf.items
      items(0) must beEqualTo((1, 0.25))
      items(1) must beEqualTo((2, 0.5))
      items(2) must beEqualTo((3, 0.75))
      items(3) must beEqualTo((4, 1.0))
    }

    "converts into a probability mass function" >> {
      val pmf = new ProbabilityMassFunction[Int]
        .set(1, 1)
        .set(2, 1)
        .set(3, 1)
        .set(4, 1)
        .normalise
        .toCdf
        .toPmf

      pmf.probability(1) must beEqualTo(0.25)
      pmf.probability(2) must beEqualTo(0.25)
      pmf.probability(3) must beEqualTo(0.25)
      pmf.probability(4) must beEqualTo(0.25)
    }

    "returns the correct value for a given probability" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq(1 -> 1.0, 2 -> 1.0, 3 -> 1.0, 4 -> 1.0))

      cdf.value(0.0) must beEqualTo(1)
      cdf.value(0.2) must beEqualTo(1)
      cdf.value(0.5) must beEqualTo(2)
      cdf.value(0.8) must beEqualTo(4)
      cdf.value(1.0) must beEqualTo(4)
    }

    "throws exception if a value is retrieved for a probability less than zero" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq(1 -> 1.0, 2 -> 1.0, 3 -> 1.0, 4 -> 1.0))
      cdf.value(-1.0) must throwAn[AssertionError]
    }

    "throws an exception if a value is retrieved for a probability greater than zero" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq(1 -> 1.0, 2 -> 1.0, 3 -> 1.0, 4 -> 1.0))
      cdf.value(2.0) must throwAn[AssertionError]
    }

    "throw a NoSuchElementException if value is called on an empty cdf" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq())
      cdf.value(0.5) must throwA[NoSuchElementException]
    }

    "select an item at random" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq(1 -> 1.0, 2 -> 1.0, 3 -> 1.0, 4 -> 1.0))
      cdf.random(rnd = new Random(1)) must beEqualTo(3)
    }

    "throw a NoSuchElementException if random is called on an empty cdf" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq())
      cdf.random must throwA[NoSuchElementException]
    }

    "select a sample at random" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq(1 -> 1.0, 2 -> 1.0, 3 -> 1.0, 4 -> 1.0))
      cdf.sample(5)(rnd = new Random(1)) must beEqualTo(Seq(3, 2, 1, 2, 4))
    }

    "find the max of k selections" >> {
      val cdf = createDice(6)
      val pmf = cdf.max(2).toPmf

      pmf.probability(1) must beCloseTo(0.02778, 0.00001)
      pmf.probability(2) must beCloseTo(0.08334, 0.00001)
      pmf.probability(3) must beCloseTo(0.13889, 0.00001)
      pmf.probability(4) must beCloseTo(0.19445, 0.00001)
      pmf.probability(5) must beCloseTo(0.25000, 0.00001)
      pmf.probability(6) must beCloseTo(0.30556, 0.00001)
    }

    "use bisect to find an approximate probability for a given value" >> {
      val cdf = new ProbabilityMassFunction[Double]
        .set(1.0, 1.0)
        .set(2.0, 1.0)
        .set(3.0, 1.0)
        .set(4.0, 1.0)
        .normalise
        .toCdf

      cdf.probability(0.5) must beEqualTo(0.0)
      cdf.probability(1.0) must beEqualTo(0.25)
      cdf.probability(1.5) must beEqualTo(0.25)
      cdf.probability(2.0) must beEqualTo(0.5)
      cdf.probability(2.5) must beEqualTo(0.5)
      cdf.probability(3.0) must beEqualTo(0.75)
      cdf.probability(3.5) must beEqualTo(0.75)
      cdf.probability(4.0) must beEqualTo(1.0)
      cdf.probability(4.5) must beEqualTo(1.0)
    }

    "return the correct cdf for a distribution containing probabilities of zero" >> {
      val cdf = CumulativeDistributionFunction[Int](Seq((1, 0.0), (2, 0.0), (3, 0.0)))

      cdf.probability(1) must beEqualTo(0.0)
      cdf.probability(2) must beEqualTo(0.0)
      cdf.probability(3) must beEqualTo(0.0)
    }

    "return the correct credible interval for a given cdf" >> {
      val cdf = Range.inclusive(0, 100).foldLeft(new ProbabilityMassFunction[Int]) {
        case (result, n) => result.set(n, 1)
      }.normalise.toCdf

      cdf.credibleInterval(100.0) must beEqualTo((0, 100))
      cdf.credibleInterval(90.0) must beEqualTo((5, 95))
      cdf.credibleInterval(70.0) must beEqualTo((15, 85))
      cdf.credibleInterval(60.0) must beEqualTo((20, 80))
      cdf.credibleInterval(50.0) must beEqualTo((25, 75))
      cdf.credibleInterval(40.0) must beEqualTo((30, 70))
      cdf.credibleInterval(30.0) must beEqualTo((35, 65))
      cdf.credibleInterval(20.0) must beEqualTo((40, 60))
      cdf.credibleInterval(10.0) must beEqualTo((45, 55))
      cdf.credibleInterval(0.0) must beEqualTo((50, 50))
    }
  }

  private def createDice(n: Int): CumulativeDistributionFunction[Int] =
    Range(1, n + 1).foldLeft(new ProbabilityMassFunction[Int]) {
      case (result, i) => result.set(i, 1 / n.toDouble)
    }.normalise.toCdf
}
