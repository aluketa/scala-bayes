package bayes.mutable

import org.specs2.matcher.MatchResult
import org.specs2.mutable.Specification

import scala.math._
import scala.util.Try

class ProbabilityMassFunctionSpec extends Specification {
  "The mutable Probability Mass Function" should {
    "set stores the correct probability" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value A", 0.25)
      pmf.set("Value B", 0.75)

      pmf.probability("Value A") must beEqualTo(0.25)
      pmf.probability("Value B") must beEqualTo(0.75)
    }

    "increment adjusts a given probability accordingly" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value A", 0.25)
      pmf.increment("Value A", 0.11)

      pmf.probability("Value A") must beEqualTo(0.25 + 0.11)
    }

    "incrementing a non existent value adds it" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.increment("Value A", 2.3)

      pmf.probability("Value A") must beEqualTo(2.3)
    }

    "multiply adjusts a given probability accordingly" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value A", 19)
      pmf.multiply("Value A", 31)

      pmf.probability("Value A") must beEqualTo(19 * 31)
    }

    "multiplying a non-existent value yields a probability of zero" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.multiply("Value A", 31)

      pmf.probability("Value A") must beEqualTo(0)
    }

    "total returns the correct value" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value A", 0.11)
      pmf.set("Value B", 0.17)
      pmf.set("Value B", 0.19)
      pmf.set("Value C", 0.31)

      pmf.total must beEqualTo(0.11 + 0.19 + 0.31)
    }

    "correctly normalises the probability values" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value A", 30)
      pmf.set("Value B", 60)
      pmf.set("Value C", 10)

      pmf.normalise()

      pmf.probability("Value A") must beEqualTo(0.3)
      pmf.probability("Value B") must beEqualTo(0.6)
      pmf.probability("Value C") must beEqualTo(0.1)

    }

    "throws NoSuchElementException if the given probability does not exist" >> {
      val pmf = new ProbabilityMassFunction[String]

      Try(pmf.probability("Non existent")) must beFailedTry.withThrowable[NoSuchElementException]
    }

    "return all items" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value A", 30)
      pmf.set("Value B", 60)
      pmf.set("Value C", 10)

      val items = pmf.items
      items must haveSize(3)
      items must contain(("Value A", 30.0))
      items must contain(("Value B", 60.0))
      items must contain(("Value C", 10.0))
    }

    "return all values" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value A", 30)
      pmf.set("Value B", 60)
      pmf.set("Value C", 10)

      val values = pmf.values
      values must haveSize(3)
      values must contain("Value A")
      values must contain("Value B")
      values must contain("Value C")
    }

    "return items in the order in which they were set" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value B", 60)
      pmf.set("Value C", 10)
      pmf.set("Value A", 30)

      pmf.items must beEqualTo(
        Seq(
          ("Value B", 60),
          ("Value C", 10),
          ("Value A", 30)
        )
      )
    }

    "return items in the order in which they were incremented" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.increment("Value B", 60)
      pmf.increment("Value C", 10)
      pmf.increment("Value A", 30)

      pmf.items must beEqualTo(
        Seq(
          ("Value B", 60),
          ("Value C", 10),
          ("Value A", 30)
        )
      )
    }

    "return items in the order in which they were multiplied" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.multiply("Value B", 60)
      pmf.multiply("Value C", 10)
      pmf.multiply("Value A", 30)

      pmf.items must beEqualTo(
        Seq(
          ("Value B", 0),
          ("Value C", 0),
          ("Value A", 0)
        )
      )
    }

    "return values in the order in which they were set" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.set("Value B", 60)
      pmf.set("Value C", 10)
      pmf.set("Value A", 30)

      pmf.values must beEqualTo(Seq("Value B", "Value C", "Value A"))
    }

    "return values in the order in which they were incremented" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.increment("Value B", 60)
      pmf.increment("Value C", 10)
      pmf.increment("Value A", 30)

      pmf.values must beEqualTo(Seq("Value B", "Value C", "Value A"))
    }

    "return values in the order in which they were multiplied" >> {
      val pmf = new ProbabilityMassFunction[String]
      pmf.multiply("Value B", 60)
      pmf.multiply("Value C", 10)
      pmf.multiply("Value A", 30)

      pmf.values must beEqualTo(Seq("Value B", "Value C", "Value A"))
    }

    "return a zero value pmf when a zero value pmf is normalized" >> {
      val pmf = new ProbabilityMassFunction[Int](Seq((1, 0.0), (2, 0.0)))
      val normalised = pmf.normalise()

      normalised.total must beEqualTo(0)
      normalised.probability(1) must beEqualTo(0)
      normalised.probability(2) must beEqualTo(0)
    }

    "return the correct items for a pmf initialised with an existing data sequence" >> {
      val pmf = new ProbabilityMassFunction[Int](Seq((1, 0.4), (2, 0.6)))
      pmf.items must beEqualTo(Seq((1, 0.4), (2, 0.6)))
    }

    "return a logarithmic pmf based on an existing pmf" >> {
      val pmf = new ProbabilityMassFunction[Int](Seq((1, 0.05), (2, 0.04), (3, 0.08), (4, 0.03)))
      val log = pmf.logarithmic

      val itemMap: Map[Int, Double] = log.items.toMap
      itemMap(1) must beCloseTo(-0.470003629, 0.000000001)
      itemMap(2) must beCloseTo(-0.693147181, 0.000000001)
      itemMap(3) must beCloseTo(-0.000000000, 0.000000001)
      itemMap(4) must beCloseTo(-0.980829253, 0.000000001)

      log.values must beEqualTo(Seq(1, 2, 3, 4))
    }

    "revert a logarithmic pmf back to a linear one" >> {
      val pmf = new ProbabilityMassFunction[Int](Seq((1, 0.05), (2, 0.04), (3, 0.08), (4, 0.03)))
        .normalise()
      val result = pmf.logarithmic.exp.normalise()

      result.values must beEqualTo(pmf.values)
      result.items.foldLeft(null.asInstanceOf[MatchResult[Double]]) {
        case (_, (value, probability)) =>
          probability must beCloseTo(pmf.probability(value), 0.000001)
      }
    }

    "allow a logarithmic pmf to be incremented" >> {
      val originalPmf =
        new ProbabilityMassFunction[Int](Seq((1, 0.05), (2, 0.04), (3, 0.08), (4, 0.03)))
          .multiply(2, 0.05)
          .normalise()

      val result =
        new ProbabilityMassFunction[Int](Seq((1, 0.05), (2, 0.04), (3, 0.08), (4, 0.03)))
          .logarithmic
          .increment(2, log(0.05))
          .exp
          .normalise()

      result.items.foldLeft(null.asInstanceOf[MatchResult[Double]]) {
        case (_, (value, probability)) =>
          probability must beCloseTo(originalPmf.probability(value), 0.000001)
      }
    }
  }
}
