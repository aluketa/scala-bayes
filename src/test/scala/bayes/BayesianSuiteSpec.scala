package bayes

import org.specs2.mutable.Specification

class BayesianSuiteSpec extends Specification {
  "An immutable BayesianSuite implementation" should {
    "return the correct probabilities for the Monty Hall problem" >> {
      val pmf = new Monty(Seq("A" -> 1, "B" -> 1, "C" -> 1)).update("B").toPmf

      pmf.probability("A") must beCloseTo(0.3333, 0.0001)
      pmf.probability("B") must beCloseTo(0.0, 0.0001)
      pmf.probability("C") must beCloseTo(0.6666, 0.0001)
    }

    "return the correct probabilities for the cookie problem" >> {
      val pmf = new Cookie().update("vanilla").toPmf

      pmf.probability("Bowl 1") must beCloseTo(0.6, 0.0001)
      pmf.probability("Bowl 2") must beCloseTo(0.4, 0.0001)
    }

    "return the correct probabilities for the M and M problem" >> {
      val pmf = new MM()
        .update(("bag1", "yellow"))
        .update(("bag2", "green"))
        .toPmf

      pmf.probability("A") must beCloseTo(0.740740, 0.000001)
      pmf.probability("B") must beCloseTo(0.259259, 0.000001)
    }

    "return the correct probabilities for the dice problem" >> {
      val pmf = new Dice(Seq(4 -> 1, 6 -> 1, 8 -> 1, 12 -> 1, 20 -> 1))
      val updated = Seq(6, 6, 8, 7, 7, 5, 4).foldLeft(pmf)((result, i) => result.update(i)).toPmf

      updated.probability(4) must beEqualTo(0.0)
      updated.probability(6) must beEqualTo(0.0)
      updated.probability(8) must beCloseTo(0.94, 0.01)
      updated.probability(12) must beCloseTo(0.05, 0.01)
      updated.probability(20) must beCloseTo(0.0015, 0.0001)
    }

    "not normalise data following update if flag is not set" >> {
      val suite = new Dice(Seq(1 -> 1, 2 -> 1))
      val posterier = suite
        .update(2, normalise = false)
        .update(2, normalise = false)
        .update(2, normalise = false)

      posterier.probability(2) must beEqualTo(0.125)
    }

    "calculate the correct probabilities using updateAll" >> {
      val pmf = new Dice(Seq(4 -> 1, 6 -> 1, 8 -> 1, 12 -> 1, 20 -> 1))
      val updated = pmf.updateAll(Seq(6, 6, 8, 7, 7, 5, 4)).toPmf

      updated.probability(4) must beEqualTo(0.0)
      updated.probability(6) must beEqualTo(0.0)
      updated.probability(8) must beCloseTo(0.94, 0.01)
      updated.probability(12) must beCloseTo(0.05, 0.01)
      updated.probability(20) must beCloseTo(0.0015, 0.0001)
    }

    "calculate the correct probabilities using the mutable path for updateAll" >> {
      val pmf = new Dice(Seq(4 -> 1, 6 -> 1, 8 -> 1, 12 -> 1, 20 -> 1))
      val updated = pmf.updateAll(Seq(6, 6, 8, 7, 7, 5, 4), useMutableIteration = true).toPmf

      updated.probability(4) must beEqualTo(0.0)
      updated.probability(6) must beEqualTo(0.0)
      updated.probability(8) must beCloseTo(0.94, 0.01)
      updated.probability(12) must beCloseTo(0.05, 0.01)
      updated.probability(20) must beCloseTo(0.0015, 0.0001)
    }

    "calculate the correct probability map using logarithmic update" >> {
      val dice = new Dice(Seq(4 -> 1, 6 -> 1, 8 -> 1, 12 -> 1, 20 -> 1))
        .logarithmic()

      val updatedPmf = Seq(6, 6, 8, 7, 7, 5, 4).foldLeft(dice)(
        (result, i) => result.logUpdate(i)).exp.normalise.toPmf

      updatedPmf.probability(4) must beEqualTo(0.0)
      updatedPmf.probability(6) must beEqualTo(0.0)
      updatedPmf.probability(8) must beCloseTo(0.94, 0.01)
      updatedPmf.probability(12) must beCloseTo(0.05, 0.01)
      updatedPmf.probability(20) must beCloseTo(0.0015, 0.0001)
    }

    "calculate the correct probability map using logarithmic update all" >> {
      val pmf = new Dice(Seq(4 -> 1, 6 -> 1, 8 -> 1, 12 -> 1, 20 -> 1))
      val updatedPmf = pmf
        .logarithmic()
        .logUpdateAll(Seq(6, 6, 8, 7, 7, 5, 4))
        .exp
        .normalise
        .toPmf

      updatedPmf.probability(4) must beEqualTo(0.0)
      updatedPmf.probability(6) must beEqualTo(0.0)
      updatedPmf.probability(8) must beCloseTo(0.94, 0.01)
      updatedPmf.probability(12) must beCloseTo(0.05, 0.01)
      updatedPmf.probability(20) must beCloseTo(0.0015, 0.0001)
    }

    "calculate the correct probability map using the mutable path for logarithmic update all" >> {
      val pmf = new Dice(Seq(4 -> 1, 6 -> 1, 8 -> 1, 12 -> 1, 20 -> 1))
      val updatedPmf = pmf
        .logarithmic()
        .logUpdateAll(Seq(6, 6, 8, 7, 7, 5, 4), useMutableIteration = true)
        .exp
        .normalise
        .toPmf

      updatedPmf.probability(8) must beCloseTo(0.94, 0.01)
      updatedPmf.probability(12) must beCloseTo(0.05, 0.01)
      updatedPmf.probability(20) must beCloseTo(0.0015, 0.0001)
    }
  }
}

class Monty(d: Seq[(String, Double)]) extends BayesianSuite[String, String] {
  override def toPmf: ProbabilityMassFunction[String] = new ProbabilityMassFunction[String](d)

  override def fromPmf(pmf: ProbabilityMassFunction[String]): TargetType = new Monty(pmf.items).asInstanceOf[TargetType]

  override def likelihood(data: String, hypothesis: String): Double = {
    if (hypothesis == data)
      0
    else if (hypothesis == "A")
      0.5
    else
      1
  }
}

class Cookie(d: Seq[(String, Double)]) extends BayesianSuite[String, String] {
  def this() = this(Seq("Bowl 1" -> 1, "Bowl 2" -> 1))

  override val toPmf: ProbabilityMassFunction[String] = new ProbabilityMassFunction[String](d)

  override def fromPmf(pmf: ProbabilityMassFunction[String]): TargetType = new Cookie(pmf.items).asInstanceOf[TargetType]

  private val mixes = Map(
    "Bowl 1" -> Map("vanilla" -> 0.75, "chocolate" -> 0.25),
    "Bowl 2" -> Map("vanilla" -> 0.5, "chocolate" -> 0.5)
  )

  override def likelihood(data: String, hypothesis: String): Double = {
    val mix = mixes(hypothesis)
    mix(data)
  }
}

class MM(d: (String, Double)*) extends BayesianSuite[String, (String, String)] {
  override def toPmf: ProbabilityMassFunction[String] = new ProbabilityMassFunction[String](d)

  override def fromPmf(pmf: ProbabilityMassFunction[String]): TargetType = new MM(pmf.items: _*).asInstanceOf[TargetType]

  def this() = this("A" -> 1, "B" -> 1)

  private val mix94 = Map("brown" -> 30, "yellow" -> 20, "red" -> 20, "green" -> 10, "orange" -> 10, "tan" -> 10)
  private val mix96 = Map("blue" -> 24, "green" -> 20, "orange" -> 16, "yellow" -> 14, "red" -> 13, "brown" -> 13)
  private val hypothesisA = Map("bag1" -> mix94, "bag2" -> mix96)
  private val hypothesisB = Map("bag1" -> mix96, "bag2" -> mix94)
  private val hypotheses = Map("A" -> hypothesisA, "B" -> hypothesisB)

  override def likelihood(data: (String, String), hypothesis: String): Double = data match {
    case (bag, colour) => hypotheses(hypothesis)(bag)(colour)
  }
}

class Dice(d: Seq[(Int, Double)]) extends BayesianSuite[Int, Int] {
  override def toPmf: ProbabilityMassFunction[Int] = new ProbabilityMassFunction[Int](d)

  override def fromPmf(pmf: ProbabilityMassFunction[Int]): TargetType = new Dice(pmf.items).asInstanceOf[TargetType]


  override def likelihood(data: Int, hypothesis: Int): Double = {
    if (hypothesis < data) {
      0.0
    } else {
      1 / hypothesis.toDouble
    }
  }
}
