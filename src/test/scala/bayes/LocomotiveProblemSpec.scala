package bayes

import org.specs2.mutable.Specification

class LocomotiveProblemSpec extends Specification {

  "performance test" >> {
    val s = System.currentTimeMillis()
    val hypotheses = (1 to 1000).map(_.toDouble)
    val pmf = BasicBayesian[Double, Double](hypotheses, (d, h) => 0).toPmf
    pmf.normalise

    println(s"Execution time: ${System.currentTimeMillis() - s}ms\n")
    (System.currentTimeMillis() - s) must beLessThan(5000L)
  }

  "return the correct mean probability for the locomotive problem" >> {
    val hypotheses = 1 to 1000
    val pmf = createTrain(hypotheses)
      .update(60)
      .toPmf

    pmf.max must beEqualTo(60)
    mean(pmf) must beCloseTo(333.420, 0.001)
  }

  "return the correct mean probability using a power law distribution for priors" >> {
    val hypotheses = 1 to 1000
    val pmf = createTrainWithPowerLaw(hypotheses, 1)
      .update(30)
      .update(60)
      .update(90)
      .toPmf

    pmf.max must beEqualTo(90)
    mean(pmf) must beCloseTo(133.275, 0.001)
  }

  "return the correct percentiles using a power law distribution for priors" >> {
    val hypotheses = 1 to 1000
    val pmf = createTrainWithPowerLaw(hypotheses, 1)
      .update(30)
      .update(60)
      .update(90)
      .toPmf

    pmf.percentile(5) must beEqualTo(91)
    pmf.percentile(95) must beEqualTo(242)
  }

  private def createTrain(hypotheses: Seq[Int]): BasicBayesian[Int, Int] =
    BasicBayesian(hypotheses, (data, hypothesis) => {
      if (hypothesis < data) {
        0.0
      } else {
        1.0 / hypothesis
      }
    })

  private def createTrainWithPowerLaw(hypotheses: Seq[Int], alpha: Double): BasicBayesian[Int, Int] = {
    val hypothesesWithPower = hypotheses.map(h => (h, Math.pow(h, -alpha)))
    new BasicBayesian(hypothesesWithPower, (data, hypothesis) => {
      if (hypothesis < data) {
        0.0
      } else {
        1.0 / hypothesis
      }
    })
  }

  private def mean(pmf: ProbabilityMassFunction[Int]): Double = {
    pmf.items.foldLeft(0.0) { case (result, value) =>
      result + (value._1 * value._2)
    }
  }
}