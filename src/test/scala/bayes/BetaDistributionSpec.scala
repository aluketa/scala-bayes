package bayes

import org.specs2.mutable.Specification

import scala.util.Random

class BetaDistributionSpec extends Specification {
  "The Beta Distribution" should {
    "return the correct mean value for a given set of inputs" >> {
      new BetaDistribution(1, 3).mean must beEqualTo(0.25)
      new BetaDistribution(1, 1).mean must beEqualTo(0.5)
      new BetaDistribution(3, 6).mean must beCloseTo(0.33333, 0.00001)
      new BetaDistribution(5, 19).mean must beCloseTo(5.0 / (19.0 + 5.0), 0.0000000001)
    }

    "return a distribution that has been updated with some given data" >> {
      val updatedBeta = new BetaDistribution(5, 7).update((11, 13))
      updatedBeta.alpha must beEqualTo(5 + 11)
      updatedBeta.beta must beEqualTo(7 + 13)
    }

    "return a random value from the distribution" >> {
      new BetaDistribution(5, 11).random(new Random(1)) must beCloseTo(0.45926, 0.00001)
      new BetaDistribution(0.1, 1).random(new Random(1)) must beCloseTo(0.96736, 0.00001)
      new BetaDistribution(0.5, 1).random(new Random(1)) must beCloseTo(0.98583, 0.00001)
      new BetaDistribution(0.8, 1).random(new Random(1)) must beCloseTo(0.98763, 0.00001)
      new BetaDistribution(1, 10).random(new Random(1)) must beCloseTo(0.03295, 0.00001)
      new BetaDistribution(1, 20).random(new Random(1)) must beCloseTo(0.01634, 0.00001)
      new BetaDistribution(10, 100).random(new Random(1)) must beCloseTo(0.12148, 0.00001)
      new BetaDistribution(50, 100).random(new Random(1)) must beCloseTo(0.37804, 0.00001)
      new BetaDistribution(90, 100).random(new Random(1)) must beCloseTo(0.51604, 0.00001)
    }

    "return a random sample from the distribution" >> {
      new BetaDistribution(5, 11).sample(4)(new Random(1)) must beEqualTo(Seq(0.45926810019628206, 0.4193610512150447, 0.3237842163821146, 0.16656663415776118))
    }

    "return the probability density for a given variable against a given distribution" >> {
      new BetaDistribution(2, 5).pdf(0.2) must beCloseTo(0.08192, 0.00001)
      new BetaDistribution(0.5, 0.5).pdf(0.5) must beCloseTo(2.0, 0.00001)
    }

    "return a PMF for a given distribution" >> {
      val pmf1 = new BetaDistribution[Double](2, 5).pmf
      val pmf2 = new BetaDistribution[Double](2, 2).pmf

      pmf1.values.takeWhile(_ < 0.42).foreach { v =>
        pmf1.probability(v) must beGreaterThan(pmf2.probability(v))
      }

      pmf1.values.dropWhile(_ < 0.42).foreach { v =>
        pmf1.probability(v) must beLessThanOrEqualTo(pmf2.probability(v))
      }

      pmf1.probability(0.5) must beCloseTo(0.00937, 0.00001)
      pmf2.probability(0.5) must beCloseTo(0.01500, 0.00001)
    }
  }
}
