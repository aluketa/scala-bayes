package bayes.kde

import org.specs2.mutable.Specification

class GaussianKernelDensityEstimateSpec extends Specification {
  "The Gaussian Kernel Density Estimate" should {
    "return the correct evaluations for the given sample" >> {
      val kde = new GaussianKernelDensityEstimate[Int](Seq(28800, 28868, 28941, 28957, 28958))

      kde.evaluate(28900) must beCloseTo(0.004303983, 0.000000001)
      kde.evaluate(28901) must beCloseTo(0.004334970, 0.000000001)
      kde.evaluate(28800) must beCloseTo(0.002278154, 0.000000001)
    }
  }
}
