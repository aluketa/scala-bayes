package bayes

import org.specs2.mutable.Specification
import bayes.Joint.tuple2PmfToJoint
import bayes.Joint.seqPmfToJoint

class JointSpec extends Specification {
  "The joint mix-in" should {
    "correctly calculate marginal values for a Tuple2 based pmf" >> {
      val pmf = new ProbabilityMassFunction[(Int, Int)]
        .set((1, 1), 1)
        .set((1, 2), 2)
        .set((2, 1), 3)
        .set((2, 2), 4)
        .normalise

      val marginalZero = pmf.marginal(0)
      marginalZero.probability(1) must beCloseTo(0.3, 0.00001)
      marginalZero.probability(2) must beCloseTo(0.7, 0.00001)

      val marginalOne = pmf.marginal(1)
      marginalOne.probability(1) must beCloseTo(0.4, 0.00001)
      marginalOne.probability(2) must beCloseTo(0.6, 0.00001)
    }

    "correctly calculate marginal values for a Seq based pmf" >> {
      val pmf = new ProbabilityMassFunction[Seq[Int]]
        .set(Seq(1, 1), 1)
        .set(Seq(1, 2), 2)
        .set(Seq(2, 1), 3)
        .set(Seq(2, 2), 4)
        .normalise

      val marginalZero = pmf.marginal(0)
      marginalZero.probability(1) must beCloseTo(0.3, 0.00001)
      marginalZero.probability(2) must beCloseTo(0.7, 0.00001)

      val marginalOne = pmf.marginal(1)
      marginalOne.probability(1) must beCloseTo(0.4, 0.00001)
      marginalOne.probability(2) must beCloseTo(0.6, 0.00001)
    }

    "correctly calculate conditional values for a Tuple2 based pmf" >> {
      val pmf = new ProbabilityMassFunction[(Int, Int)]
        .set((1, 1), 1)
        .set((1, 2), 2)
        .set((2, 1), 3)
        .set((2, 2), 4)
        .normalise

      val conditional = pmf.conditional(0, 1, 2)
      conditional.probability(1) must beCloseTo(2.0 / 6.0, 0.00001)
      conditional.probability(2) must beCloseTo(4.0 / 6.0, 0.00001)
    }

    "correctly calculate conditional values for a Sequence based pmf" >> {
      val pmf = new ProbabilityMassFunction[Seq[Int]]
        .set(Seq(1, 1), 1)
        .set(Seq(1, 2), 2)
        .set(Seq(2, 1), 3)
        .set(Seq(2, 2), 4)
        .normalise

      val conditional = pmf.conditional(0, 1, 2)
      conditional.probability(1) must beCloseTo(2.0 / 6.0, 0.00001)
      conditional.probability(2) must beCloseTo(4.0 / 6.0, 0.00001)
    }

    "correctly calculate a joint distribution from two pmfs" >> {
      val pmf1 = new ProbabilityMassFunction[Int]
        .set(1, 0.25)
        .set(2, 0.5)
        .set(3, 0.05)
        .set(4, 0.2)

      val pmf2 = new ProbabilityMassFunction[Int]
        .set(1, 0.2)
        .set(2, 0.05)
        .set(3, 0.5)
        .set(4, 0.25)

      val joint = Joint.from(pmf1, pmf2)
      joint.targetPmf.probability((1, 1)) must beCloseTo(0.25 * 0.2, 0.00001)
      joint.targetPmf.probability((1, 2)) must beCloseTo(0.25 * 0.05, 0.00001)
      joint.targetPmf.probability((1, 3)) must beCloseTo(0.25 * 0.5, 0.00001)
      joint.targetPmf.probability((1, 4)) must beCloseTo(0.25 * 0.25, 0.00001)
      joint.targetPmf.probability((2, 1)) must beCloseTo(0.5 * 0.2, 0.00001)
      joint.targetPmf.probability((2, 2)) must beCloseTo(0.5 * 0.05, 0.00001)
      joint.targetPmf.probability((2, 3)) must beCloseTo(0.5 * 0.5, 0.00001)
      joint.targetPmf.probability((2, 4)) must beCloseTo(0.5 * 0.25, 0.00001)
      joint.targetPmf.probability((3, 1)) must beCloseTo(0.05 * 0.2, 0.00001)
      joint.targetPmf.probability((3, 2)) must beCloseTo(0.05 * 0.05, 0.00001)
      joint.targetPmf.probability((3, 3)) must beCloseTo(0.05 * 0.5, 0.00001)
      joint.targetPmf.probability((3, 4)) must beCloseTo(0.05 * 0.25, 0.00001)
      joint.targetPmf.probability((4, 1)) must beCloseTo(0.2 * 0.2, 0.00001)
      joint.targetPmf.probability((4, 2)) must beCloseTo(0.2 * 0.05, 0.00001)
      joint.targetPmf.probability((4, 3)) must beCloseTo(0.2 * 0.5, 0.00001)
      joint.targetPmf.probability((4, 4)) must beCloseTo(0.2 * 0.25, 0.00001)

    }
  }
}
