package bayes.pdf

import bayes.pdf.PoissonProbabilityDensityFunction.poissonPmf
import org.specs2.mutable.Specification

class PoissonProbabilityDensityFunctionSpec extends Specification {
  "The Poisson PDF" should {
    "return the correct evaluation for a poisson pmf point" >> {
      val pdf = new PoissonProbabilityDensityFunction(6.5)
      pdf.density(0) must beCloseTo(0.00150, 0.00001)
      pdf.density(1) must beCloseTo(0.00977, 0.00001)
      pdf.density(2) must beCloseTo(0.03176, 0.00001)
      pdf.density(3) must beCloseTo(0.06881, 0.00001)
      pdf.density(4) must beCloseTo(0.11182, 0.00001)
      pdf.density(5) must beCloseTo(0.14536, 0.00001)
      pdf.density(6) must beCloseTo(0.15748, 0.00001)
      pdf.density(7) must beCloseTo(0.14623, 0.00001)
      pdf.density(8) must beCloseTo(0.11881, 0.00001)
      pdf.density(9) must beCloseTo(0.08581, 0.00001)
      pdf.density(10) must beCloseTo(0.05577, 0.00001)
    }

    "return the correct evaluation of a large density" >> {
      val pdf = new PoissonProbabilityDensityFunction(200)
      pdf.density(200) must beCloseTo(0.02819, 0.00001)
    }

    "return a pmf based on a poisson distribution" >> {
      val pmf = poissonPmf(6.5, 0, 10)
      pmf.size must beEqualTo(11)
      pmf.probability(0) must beCloseTo(0.00161, 0.00001)
      pmf.probability(1) must beCloseTo(0.01047, 0.00001)
      pmf.probability(2) must beCloseTo(0.03403, 0.00001)
      pmf.probability(3) must beCloseTo(0.07374, 0.00001)
      pmf.probability(4) must beCloseTo(0.11983, 0.00001)
      pmf.probability(5) must beCloseTo(0.15578, 0.00001)
      pmf.probability(6) must beCloseTo(0.16876, 0.00001)
      pmf.probability(7) must beCloseTo(0.15670, 0.00001)
      pmf.probability(8) must beCloseTo(0.12732, 0.00001)
      pmf.probability(9) must beCloseTo(0.09195, 0.00001)
      pmf.probability(10) must beCloseTo(0.05977, 0.00001)
    }
  }
}
