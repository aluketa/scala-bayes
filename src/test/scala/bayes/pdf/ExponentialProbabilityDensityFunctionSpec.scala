package bayes.pdf

import bayes.pdf.ExponentialProbabilityDensityFunction.exponentialPmf
import org.specs2.mutable.Specification

class ExponentialProbabilityDensityFunctionSpec extends Specification {
  "The Exponential Probability Density Function" should {
    "return the correct values for a given set of inputs" >> {
      val poissonPdf = new PoissonProbabilityDensityFunction(6.5)
      val exponentialPdf = new ExponentialProbabilityDensityFunction[Double](6.5)
      exponentialPdf.density(poissonPdf.density(0)) must beCloseTo(6.43678905702351, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(1)) must beCloseTo(6.09995790904126, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(2)) must beCloseTo(5.28758268838761, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(3)) must beCloseTo(4.15583493913021, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(4)) must beCloseTo(3.14230888437853, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(5)) must beCloseTo(2.52667584381748, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(6)) must beCloseTo(2.33535303780649, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(7)) must beCloseTo(2.51250472107914, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(8)) must beCloseTo(3.00267342051533, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(9)) must beCloseTo(3.72114185799011, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(10)) must beCloseTo(4.52333988631132, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(11)) must beCloseTo(5.24653140875441, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(12)) must beCloseTo(5.78783451384038, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(13)) must beCloseTo(6.13358984119108, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(14)) must beCloseTo(6.32723543430638, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(15)) must beCloseTo(6.42456360540519, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(16)) must beCloseTo(6.46924772052845, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(17)) must beCloseTo(6.48822455181273, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(18)) must beCloseTo(6.49574529157497, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(19)) must beCloseTo(6.49854412833148, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(20)) must beCloseTo(6.49952680593564, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(21)) must beCloseTo(6.4998535314893, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(22)) must beCloseTo(6.49995672486923, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(23)) must beCloseTo(6.49998777004253, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(24)) must beCloseTo(6.49999668771758, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(25)) must beCloseTo(6.49999913880641, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(26)) must beCloseTo(6.49999978470159, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(27)) must beCloseTo(6.4999999481689, 0.00000000000001)
      exponentialPdf.density(poissonPdf.density(28)) must beCloseTo(6.49999998796778, 0.00000000000001)
    }

    "return a pmf based on an exponential distribution" >> {
      val pmf = exponentialPmf(6.5, 0, 1, 5)
      pmf.probability(0) must beCloseTo(0.803326145, 0.000000001)
      pmf.probability(0.25) must beCloseTo(0.158184297, 0.000000001)
      pmf.probability(0.5) must beCloseTo(0.031148335, 0.000000001)
      pmf.probability(0.75) must beCloseTo(0.006133471, 0.000000001)
      pmf.probability(1) must beCloseTo(0.001207752, 0.000000001)
    }
  }
}
