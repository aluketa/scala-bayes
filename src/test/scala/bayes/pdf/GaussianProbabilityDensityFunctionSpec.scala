package bayes.pdf

import org.specs2.mutable.Specification
import bayes.pdf.GaussianProbabilityDensityFunction.gaussianPmf

import scala.math.log

class GaussianProbabilityDensityFunctionSpec extends Specification {
  "Gaussian Probability Density Function" should {
    "return the correct values for a given set of inputs" >> {
      new GaussianProbabilityDensityFunction[Int](0, 1).density(-2) must beCloseTo(0.053990, 0.000001)
      new GaussianProbabilityDensityFunction[Int](0, 1).density(-1) must beCloseTo(0.241970, 0.000001)
      new GaussianProbabilityDensityFunction[Int](0, 1).density(0) must beCloseTo(0.398942, 0.000001)
      new GaussianProbabilityDensityFunction[Int](0, 1).density(1) must beCloseTo(0.241970, 0.000001)
      new GaussianProbabilityDensityFunction[Int](0, 1).density(2) must beCloseTo(0.053991, 0.000001)

      new GaussianProbabilityDensityFunction[Int](10, 100).density(-20) must beCloseTo(0.003813, 0.000001)
      new GaussianProbabilityDensityFunction[Int](10, 100).density(-10) must beCloseTo(0.0039104, 0.000001)
      new GaussianProbabilityDensityFunction[Int](10, 100).density(0) must beCloseTo(0.003969, 0.000001)
      new GaussianProbabilityDensityFunction[Int](10, 100).density(10) must beCloseTo(0.003989, 0.000001)
      new GaussianProbabilityDensityFunction[Int](10, 100).density(20) must beCloseTo(0.003969, 0.000001)
    }

    "return a pmf based on a gaussian distribution" >> {
      val pmf = gaussianPmf(2.7, 0.3, 4)

      pmf.items must haveSize(100)
      pmf.items(0)._2 must beLessThan(pmf.items(50)._2)
      pmf.items(50)._2 must beGreaterThan(pmf.items(99)._2)
    }

    "return the correct log value of a gaussian density" >> {
      log(new GaussianProbabilityDensityFunction[Int](10, 100).density(0)) must beCloseTo(-5.529108, 0.000001)
      log(new GaussianProbabilityDensityFunction[Int](10, 100).density(10)) must beCloseTo(-5.524108, 0.000001)
      log(new GaussianProbabilityDensityFunction[Int](10, 100).density(20)) must beCloseTo(-5.529108, 0.000001)
    }
  }
}
