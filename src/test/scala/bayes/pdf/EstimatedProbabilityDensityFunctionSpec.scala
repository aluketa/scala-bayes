package bayes.pdf

import org.specs2.mutable.Specification

class EstimatedProbabilityDensityFunctionSpec extends Specification {
  "The Estimated PDF" should {
    "return the correct probabilities for a given sample" >> {
      val estimatedPdf = new EstimatedProbabilityDensityFunction[Int](Seq(28800, 28868, 28941, 28957, 28958))
      val pmf = estimatedPdf.toPmf(Seq(28900, 28901, 28800))

      pmf.probability(28800) must beCloseTo(0.20867, 0.00001)
      pmf.probability(28900) must beCloseTo(0.39424, 0.00001)
      pmf.probability(28901) must beCloseTo(0.39708, 0.00001)
    }
  }
}
